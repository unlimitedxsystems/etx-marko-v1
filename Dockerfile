FROM node:12-alpine
ENV NODE_ENV=production
MAINTAINER Enterstarts info@enterstarts.com 

COPY . /opt/etx-marko-v1
WORKDIR /opt/etx-marko-v1
RUN npm install --production

CMD ["npm", "run", "dev"] 
