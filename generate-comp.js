const _=require('underscore');
const moment=require('moment');
const fs = require('fs');
const exec = require('child_process').exec;

function parseArguments() {
    const result = Object.create(null);
    process.argv.forEach((argument) => {
        if (argument.indexOf('=') != -1) {
            const index = argument.indexOf('=');
            const name = argument.substr(0, index);
            const value = argument.substr(index + 1);
            result[name] = value;
        } else {
            result[argument] = true;
        }
    });    
    return result;
}

const args=parseArguments();
var codename=args.name;

if (!codename) {
    console.error("Usage: node generate-module.js name=<codename>");
    process.exit(1);
}

if (codename.match(/[^a-z0-9\-]/g)) {    
    console.error("Error: Name must be cased (my-component), And the first caracter must a alphabet");
    process.exit(1);
}

if (codename[0].match(/[^a-z]/g)) {    
    console.error("Error: First caracter must a alphabet");
    process.exit(1);
}

var comptitle=args.title || args.modname;
var path="./src/components/";
console.info("compname: ", codename);

_.templateSettings = {
  interpolate: /\{\{(.+?)\}\}/g
};

generateComp();

function generateComp() {
    var compiled=`<div class="row rs-margin">
    <div class="col-12">
        <h1>${comptitle||""}</h1>
        Component ${codename}
    </div>
</div>`.trim();
    console.info("NEW-COMPONENT REQUEST %s", compiled);

    // var compiled = _.template(str);
    // var module_js = compiled({
    //     codename: codename,
    // });

    var tpl_path=path+codename+"/index.marko";
    if (fs.existsSync(tpl_path) && args.force!=="true") {
        console.error("[ERROR] Component already exists[force=true to replace] [%s]", tpl_path)
        return;
    } else {
        console.error("[INFO] Component not registered, proceding [%s]", tpl_path)

    }

    var folder_name=path+codename;
    console.info("[INFO] creating folder %s", folder_name);
    var cmd='mkdir -p '+ folder_name;

    execCmd(cmd)
    .then(function (res) {
        var module_file=folder_name+"/index.marko";
        console.info("[INFO] Created folder %s", folder_name);
        console.info("[INFO] Creating component file %s", module_file);

        writeToFile(module_file, compiled)
        .then(LOGFILE, LOGERROR);

    }, function (err) {
        console.error("[ERROR] Failed to created folder [%s] ==> %o ", folder_name, err);
    });
}

function LOGFILE(file) {
    // body...
    console.info("Created ", file);
}

function LOGERROR(argument) {
    // body...
    console.error("Error: ", argument);
}

function writeToFile(filename, data) {
    return new Promise(function (resolve, reject) {
        // body...
        fs.writeFile(filename, data, function(err, data){
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        }, reject);
    });
}

function execCmd(cmd) {
    return new Promise(function(resolve, reject) {
        exec(cmd, function(err, stdout) {
            if (err) return reject(err);
            resolve(stdout);
        });
    });
}
