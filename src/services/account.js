const _=require('underscore');
const moment=require('moment');
const modsConf = require('../api/modules-conf');
const APIService = require('../api/service').apiService;

function fromToken(Auth) {
    var api=new APIService("user", Auth);
    return api.get("me");
}

function mobileSignin (BotAuth, reqObj) {
    return new Promise(function (resolve, reject) {
        var api=new APIService("user", BotAuth);
        api.post("signin", reqObj)
        .then(function (resp) {
            resolve(resp);
        }, reject);
    });
}

exports.fromToken=fromToken;
exports.mobileSignin=mobileSignin;