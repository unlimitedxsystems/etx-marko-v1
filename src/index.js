import express from "express";
import compressionMiddleware from "compression";
import markoMiddleware from "@marko/express";
import indexPage from "./pages/index";
import signinPage from "./pages/auth/signin";
import aboutPage from "./pages/about";
import builderPage from "./pages/builder-v1";
import apiGatewayPage from "./pages/api-gateway";
import componentBuilderPage from "./pages/component-builder";
import accountPage from "./pages/account";
import {RenderPage} from './webapp/module';
import applicationCloudPage from './pages/application-cloud';
import {etxTables,APP_MODE,conf} from './api/modules-conf';
import * as compiler from "@marko/compiler";
const accountService=require("./services/account");

// import marketplacePage from "./pages/marketplace";
// import usersService from "./services/users";

var bodyParser = require('body-parser');
const auth=require('./api/auth');

const port = parseInt(process.env.PORT || 3000, 10);

var user = {
    email: 'public@enterstarts.com',
    password: '@CHCg592)6cVzQ!&.E%',
    apiKey: 'kqCr1ArBcPsrRzlg7EBHuAW/FaS151OSWknB0lVj8Cw='
};
const urlencodedParser = bodyParser.urlencoded({ 
    extended: true 
});
var app=express();

// almada_token
// const AUTH_TOKEN='XBTFINEX eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ7XCJpZFwiOlwiNTc0MWQxMmIyNjI2NDU0NVwiLFwiaWRTZXFcIjoxOTIsXCJuYW1lXCI6XCJBeXJ0b25cIixcImVtYWlsXCI6XCJheXJ0b25hZG1AZW50ZXJzdGFydHMuY29tXCIsXCJwYXNzd29yZENvbmZcIjpcIioqKioqKioqKlwiLFwicGFzc3dvcmRPbGRcIjpcIioqKioqKioqKipcIixcImxhbmdcIjpcInB0XCIsXCJwaWN0dXJlXCI6bnVsbCxcImxvY2F0aW9uXCI6XCJ3b3JsZHdpZGVcIixcInN0YXRlXCI6XCJDT05GSVJNRURcIixcInJlZmVycmFsQ29kZVwiOlwiYXlydG9uLTc2ZmI4XCIsXCJvcmdcIjpcImUyMmMzOTRiMzFjNzQwNjNcIixcImNyZWF0ZWRBdFwiOlwiMjAyMC0wNC0zMCAxMTo0Mjo0M1wiLFwiY3JlYXRlZEJ5XCI6XCI3OTczZWE0ZjZlYTc0MWJlXCIsXCJkZWxldGVkXCI6ZmFsc2UsXCJzY2hlbWFSZWZcIjpcIjI3MGZhNDQ5YWI5MjRkMDRcIixcInR5cGVcIjpcIkFETUlOXCIsXCJyZXF1aXJlc0NhcHRjaGFcIjp0cnVlLFwidmlhQXBwXCI6bnVsbCxcInZpYU9yZ1wiOm51bGwsXCJwaW5DaGFubmVsXCI6bnVsbCxcImJpcnRoZGF0ZVwiOm51bGwsXCJnZW5kZXJcIjpudWxsLFwicmVnaW9uXCI6bnVsbCxcInBob25lXCI6XCIwMDBcIixcInBob25lQ29kZVwiOm51bGwsXCJiaW9cIjpudWxsLFwiZG9jSWRcIjpudWxsLFwiem9uZVwiOm51bGwsXCJ6b25lTGFiZWxcIjpudWxsLFwiaWxoYVwiOm51bGwsXCJjb25jZWxob1wiOm51bGwsXCJwcm9mZXNzaW9uXCI6bnVsbCxcInByb2Zlc3Npb25MYWJlbFwiOm51bGwsXCJjaXR5XCI6bnVsbCxcInV0SWRcIjpudWxsLFwiZG9jTG9jYXRpb25cIjpudWxsLFwic2VxTnVtYmVyXCI6bnVsbCxcImFjY291bnREYXRhXCI6bnVsbCxcInZpc2liaWxpdHlcIjpcIlBST1RFQ1RFRFwifSIsImV4cCI6MTY0NTMyNzUzMX0.gloPIwkq8qe0pFINnE33GAfBZkDYypu3WuyfWliIu8s'
// kualeh_token
const AUTH_TOKEN='XBTFINEX eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ7XCJpZFwiOlwiZjMyNmQ0ZjU2Yzc2NDlkY1wiLFwiaWRTZXFcIjo1MDEsXCJuYW1lXCI6XCJBRE1JTlwiLFwiZW1haWxcIjpcInVuaXRlbC1wb2NAZW50ZXJzdGFydHMuY29tXCIsXCJwYXNzd29yZENvbmZcIjpcIioqKioqKioqKlwiLFwicGFzc3dvcmRPbGRcIjpcIioqKioqKioqKipcIixcImxhbmdcIjpcImVuXCIsXCJwaWN0dXJlXCI6XCJodHRwczovL2V0eC1zdG9yYWdlLnMzLnVzLWVhc3QtMi5hbWF6b25hd3MuY29tL1BST0ZJTEVfRk9UTy9mMzI2ZDRmNTZjNzY0OWRjLzI2MGVlMDQ0YmQxMjM0YzllN2Y1NWY2YWU3OTcxMGJlLmpwZ1wiLFwibG9jYXRpb25cIjpcIndvcmxkd2lkZVwiLFwic3RhdGVcIjpcIk5PVF9DT05GSVJNRURcIixcInJlZmVycmFsQ29kZVwiOlwidW5pdGVsLTZlYzgwXCIsXCJvcmdcIjpcImQxOTcxODMzMGJiYjQzODlcIixcImNyZWF0ZWRBdFwiOlwiMjAyMC0wOS0xOCAxNTo0MzoxMFwiLFwiY3JlYXRlZEJ5XCI6XCJjZDM5MGQ3MWVlNDA0M2Q2XCIsXCJkZWxldGVkXCI6ZmFsc2UsXCJzY2hlbWFSZWZcIjpcIjdjZWUzYzcxZDlkNzQ1ZjZcIixcInR5cGVcIjpcIkFETUlOXCIsXCJyZXF1aXJlc0NhcHRjaGFcIjp0cnVlLFwidmlhQXBwXCI6bnVsbCxcInZpYU9yZ1wiOm51bGwsXCJwaW5DaGFubmVsXCI6bnVsbCxcImJpcnRoZGF0ZVwiOm51bGwsXCJnZW5kZXJcIjpudWxsLFwicmVnaW9uXCI6bnVsbCxcInBob25lXCI6bnVsbCxcInBob25lQ29kZVwiOm51bGwsXCJiaW9cIjpudWxsLFwiZG9jSWRcIjpudWxsLFwiem9uZVwiOm51bGwsXCJ6b25lTGFiZWxcIjpudWxsLFwiaWxoYVwiOm51bGwsXCJjb25jZWxob1wiOm51bGwsXCJwcm9mZXNzaW9uXCI6bnVsbCxcInByb2Zlc3Npb25MYWJlbFwiOm51bGwsXCJjaXR5XCI6bnVsbCxcInV0SWRcIjpudWxsLFwiZG9jTG9jYXRpb25cIjpudWxsLFwic2VxTnVtYmVyXCI6bnVsbCxcInZpc2liaWxpdHlcIjpcIlBST1RFQ1RFRFwiLFwiYWNjb3VudERhdGFcIjpudWxsfSIsImV4cCI6MTY0NTM4MzM1OH0.sQX04L5aRUt0IRe02lZlQ2grdJjU7tCxLXarkTPmYDg';

app.use(urlencodedParser);

app.use("/static/", express.static("/home/ayrton/env/git/99app/etx-v2/etx-marko-v1/static/"));

app.use(bodyParser.json());
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, delete");
    res.header("Access-Control-Allow-Methods", "GET, POST, HEAD, PUT, DELETE, OPTIONS");
    next();
});

app.use(compressionMiddleware()) // Enable gzip compression for all HTTP responses.
app.use("/assets", express.static("dist/assets")) // Serve assets generated from webpack.
app.use(markoMiddleware()) // Enables res.marko.

// *************
// ** Routes **
// *************

app.get("/", indexPage);

app.get("/account", accountPage);
app.get("/signin", signinPage);
app.get("/component-builder/:appId", function (req, res) {
    req.headers["authorization"]=AUTH_TOKEN;
    componentBuilderPage(req, res);
});
app.get("/application-builder", componentBuilderPage);

app.get("/builder-v1", builderPage);
app.get("/api-gateway", apiGatewayPage);

app.post("/me", function (req, res) {
    var userReq=req.body;
    if (!userReq) {
        res.status(400)
            .send({"errors": "INVALID_REQUEST"});
        return;            
    }

    accountService.fromToken(userReq.token)
    .then(function (Resp) {        
        res.status(200)
            .send({
                id: Resp.id,
                email: Resp.email,
                org: Resp.org,
                name: Resp.name,
                type: Resp.type,
                state: Resp.state
            });

    }, function (Err) {
        var status=Err.data&&Err.data.status ? Err.data.status : 400;
        res.status(status)
            .send(Err);
    });
});

app.post("/authenticate", function (req, res) {
    var userReq=req.body;
    if (!userReq) {
        res.status(400)
            .send({"errors": "INVALID_REQUEST"});
        return;            
    }

    accountService.mobileSignin("", userReq)
    .then(function (Resp) {        
        res.status(200)
            .send(Resp);

    }, function (Err) {
        var status=Err.data&&Err.data.status ? Err.data.status : 400;
        res.status(status)
            .send(Err);
    });
});

app.get("/render/:appId/:env/:lang/:page", function (req, res) {
    var autori=req.headers['authorization'];
    var context = {
        appId: req.params.appId,
        env: req.params.env,
        lang: req.params.lang,
        page: req.params.page
    }

    RenderPage(BOT_AUTH.reqToken, autori, context, req, res)
    .then(function (compiled) {   

        if (context.page=="test-error") {        
            res.status(400)
            .send({
                errors: ["Bad request"]
            });
            return;
        }

        setTimeout(function () {
          res.send(compiled);
        }, 400);

    }, function (error) {
        console.error("RenderPage()::Error:: ", error);
        res.status(400)
        .send({
            "errors": error
        });
    });
})

app.get("/application-cloud", function (req, res) {
    req.headers["authorization"]=AUTH_TOKEN;
    applicationCloudPage(req, res);
});

app.get("/integration-api", function (req, res) {

});


console.info("============");
console.info("ENTERSTARTS MARKO ENGINE [1.0]");
console.info("APP-MODE: ", APP_MODE().toUpperCase());
console.info("SERVER AT " + conf.authAPI());
console.info("Login in with ", user.email);
console.info("============");

const SERVER_PORT=9002;

var BOT_AUTH;
auth.login(user)
.then(function (Auth) {
    BOT_AUTH=Auth;
    console.info("%s[%s] logged in", Auth.user.name, Auth.user.email);

    app.listen(SERVER_PORT, function (port, err) {
        if (err) {
          throw err;
        }

        console.log('started server on *:'+SERVER_PORT);
    });

}, function (Err) {
    console.error("Could not login user %s[%s]", user.name, user.email);
    console.error("Details: %o", JSON.stringify(Err));
});

setInterval(function() {
    auth.login(user)
    .then(function  (Auth) {
        BOT_AUTH=Auth;
        console.info("%s[%s] re-logged in", user.name, user.email);

        // connect again to aerospike
        // Aes.connect(config)
        // .then(client => {
        //     console.info("======> Connected to aerospike at ", config.hosts);
        //     AES_CLIENT=client;
        // });

    }, function (Err) {
        console.error("Could not re-login user %s[%s]", user.name, user.email);
        console.error("Details: %o", JSON.stringify(Err));
    });
}, 90 * 60 * 1000); // re-login each 90 minutes
