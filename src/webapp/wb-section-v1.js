const modsConf=require('../api/modules-conf');
const APIService=require("../api/service").apiService;
const wbSection=require("./wb-section");
const moment=require('moment');
const _=require('underscore');

const exec = require('child_process').exec;
const fs = require('fs');
const wbUtils=require("./utils");

var WebApp=modsConf.c.WebAppTable();
var waFields=WebApp.fields();

var WebAppPage=modsConf.c.WebAppPage();
var wapFields=WebAppPage.fields();
var wapCols=Object.keys(wapFields).map(function (C) {
    return wapFields[C];
});

var PageSection=modsConf.c.WebPageSections();
var psFields=PageSection.fields();
var psCols=Object.keys(psFields).map(function (C) {
    return psFields[C];
});

var PageComp=modsConf.c.WebPageComp();
var pcFields=PageComp.fields();
var pcCols=Object.keys(pcFields).map(function (C) {
    return pcFields[C];
});

const POST_PLACEHOLDER="___post_html____";

var WebApp=modsConf.c.WebAppTable();
var waFields=WebApp.fields();

var WebAppTpl=modsConf.c.WebAppTplTable();
var wptFields=WebAppTpl.fields();

var WebAppPage=modsConf.c.WebAppPage();
var wapFields=WebAppPage.fields();
var wapCols=Object.keys(wapFields).map(function (C) {
    return wapFields[C];
});

var PageSection=modsConf.c.WebPageSections();
var psFields=PageSection.fields();
var psCols=Object.keys(psFields).map(function (C) {
    return psFields[C];
});

var PageComp=modsConf.c.WebPageComp();
var pcFields=PageComp.fields();

var ServiceTbl=modsConf.c.ServiceTbl();
var svFields=ServiceTbl.fields();
var svCols=Object.keys(svFields).map(function (F){ 
    return svFields[F];
});

var BlogSystem=modsConf.c.BlogSystem();
var bxFields=BlogSystem.fields();
var bxCols = Object.keys(bxFields).map(function (K) {
    return bxFields[K];
});

var Entity=modsConf.c.EntityTable();
var entFields=Entity.fields();
var entCols=Object.keys(entFields).map(function (K) {
    return entFields[K];
});

var Inventory=modsConf.c.InventoryTable();
var invtFields=Inventory.fields();
var invtCols=Object.keys(invtFields).map(function (C) {
    return invtFields[C];
});

var EcommerceCart=modsConf.c.EcommerceCart();
var ecFields=EcommerceCart.fields();
var ecCols=Object.keys(ecFields)
    .map(function (F){ 
        return ecFields[F];
    });

var ObjectTags=modsConf.c.ObjectTags();
var otFields=ObjectTags.fields();
var otCols=Object.keys(otFields)
    .map(function (F){ 
        return otFields[F];
    });

var ProductMedia=modsConf.c.ProductMedia();
var pmeFields=ProductMedia.fields();
var pmeCols=Object.keys(pmeFields)
    .map(function (F){ 
        return pmeFields[F];
    });

var Billing=modsConf.c.Billing();
var blFields=Billing.fields();
var blCols=Object.keys(blFields)
    .map(function (F){ 
        return blFields[F];
    });

var Merchant=modsConf.c.MerchantAccount();
var mcFields=Merchant.fields();
var mcCols=Object.keys(mcFields)
    .map(function (F){ 
        return mcFields[F];
    });

var rsHost=modsConf.c.HostTbl();
var rsHostFields=rsHost.fields();
var rsHostCols=Object.keys(rsHostFields)
    .map(function (F){ 
        return rsHostFields[F];
    });

var Vinti4TxResponse=modsConf.c.Vinti4TxResponse();
var vt4Fields=Vinti4TxResponse.fields();
var vt4Cols=Object.keys(vt4Fields)
    .map(function (F){ 
        return vt4Fields[F];
    });

var Reviews=modsConf.c.ReviewsTable();
var rvFields=Reviews.fields();
var rvCols=Object.keys(rvFields)
    .map(function (F){ 
        return rvFields[F];
    });
    
function getPageLocation(page) {
    var name=page.fields[wapFields.name];
    return wbUtils.normalizePageName(name) + ".html"
}

var support_cache={};

function generatePage(Auth, orgObj, webapp, page, page_list, sections, components, should_store, Locale, env) {
    var webappTitle=webapp.fields[waFields.title_df];
    var logo=webapp.fields[waFields.image];
    var cloudfront_dist=webapp.fields[waFields.cloudfront_dist];
    var cover=webapp.fields[waFields.cover];
    var mobile_url=webapp.fields[waFields.mobile_url];
    var mobile_id=webapp.fields[waFields.mobile_id];
    var origin=webapp.fields[waFields.origin];
    var domain=webapp.fields[waFields.domain];

    var pageTitle=page.fields[wapFields.title_df];
    var pageDescr=(page.fields[wapFields.descr_df]+"").replace(/[\n\t]/g, '');
    var pageAuthor=(page.fields[wapFields.pg_author]+"").replace(/[\n\t]/g, '');;
    var pageKeywords=(page.fields[wapFields.pg_keywords]+'').replace(/[\n\t]/g, '');;
    var pageJS=page.fields[wapFields.tracking_js] || '';
    var pageHEAD=page.fields[wapFields.head_code] || '';
    var today=moment().format("YYYY-MM-DD HH:mm");
    var staticPath=modsConf.c.PUBLICA_HOST_CDN();

    env = env || 'PROD';
    var env_path = (!env||env=='PROD')? "" : "/"+env;
    var env_label = ((env=='PROD')? 'production' : env).toLowerCase();

    if (modsConf.APP_MODE()!="online") {
        staticPath=modsConf.c.STATIC_PATH();
    }

    var pageStyle='';

    // console.info("Page %s has %d components", 
    //     page.fields[wapFields.name], 
    //     components.length
    // );
    var scount=0;

    var navbar_comp='';
    var footer_comp='';
    var carrs_comp='';

    // GENERATE COMPONENT HTML
    // XXX: blog-system
    var proms=[];
    var comp_list_sorted=_.sortBy(components, function (C) {
        var order=parseInt(C.fields[pcFields.order]||0);
        if (isNaN(order)) {
            order=scount++;
        }

        proms.push(generateComp(Auth, orgObj, webapp, page, C, page_list, sections, Locale, env));
        // proms.push(Promise.resolve("<h1>Component "+C.id+"</h1>"));
        // C.compiled_html="<h1>Component "+C.id+"</h1>";
        // proms.push(Promise.resolve(C));
        return order;
    });

    return new Promise(function (resolve, reject) {
        Promise.all(proms)
        .then(function (comp_list_el) {
            var comp_list = comp_list_el.map(function (C) {
                C.comp.compiled_html=C.compiled_html;
                return C.comp;
                // return C;
            });

            var nidx=-1, fidx=-1, cidx=-1;
            comp_list.forEach(function (C, Idx) {
                var comp_type=C.fields[pcFields.type];
                var section_id=C.fields[pcFields.section_id];
                if (section_id=='GLOBAL_SECTION'&&comp_type=='SITE_NAVBAR') {            
                    navbar_comp=C;

                } else if (section_id=='GLOBAL_SECTION'&&comp_type=='SITE_FOOTER') {
                    footer_comp=C;

                } else if (section_id=='GLOBAL_SECTION'&&comp_type=='SITE_CARROUSEL') {
                    carrs_comp=C;
                }
            });

            comp_list=_.sortBy(comp_list, function (C) {
                return parseInt(C.fields[pcFields.order]);
            });

            var appName=webapp.fields[waFields.title_df];
            var head_html=`
            <head>
                <title>${(pageTitle)}</title>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <meta name="description" content="${(pageDescr)}" />
                <meta name="keywords" content="${pageKeywords}" />
                <meta name="author" content="${pageAuthor}" />
                <meta name="ETX-Careers" content="Is code your craft? -> https://cloud.enterstarts.com/careers.html" />
                <meta name="ETX-WebApp-Name" content="${appName}" />
                <meta name="ETX-WebApp-Id" content="${webapp.id}" />
                <meta name="ETX-WebApp-Env" content="${env_label}" />
                <meta name="ETX-WebApp-Org" content="${webapp.org}" />
                <meta name="ETX-Page-Id" content="${page.id}" />
                <meta name="ETX-Page-Lang" content="" />
                <meta name="ETX-WebApp-Generated" content="${today}" />
                <meta name="ETX-Logo" content="${logo}" />
                <meta name="ETX-Cover" content="${cover}" />
                <meta name="ETX-cloudfront_dist" content="${cloudfront_dist}" />
                <meta name="ETX-mobile_url" content="${mobile_url}" />
                <meta name="ETX-mobile_id" content="${mobile_id}" />
                <meta name="ETX-origin" content="${origin}" />
                <meta name="ETX-domain" content="${domain}" />
                <link rel="shortcut icon" href="${logo}" type="image/x-icon" />

                <!--
                    <link href="https://vjs.zencdn.net/7.10.2/video-js.min.css" rel="stylesheet">
                    <link rel="stylesheet" href="https://unpkg.com/@videojs/themes@1/dist/city/index.css" />
                    <link rel="stylesheet" type="text/css" href="${staticPath}/vendor/js-socials/jssocials.css" />
                    <link rel="stylesheet" type="text/css" href="${staticPath}/vendor/jssocials-theme-flat.css" />
                -->

                <link href="${staticPath}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
                <link href="${staticPath}/css/jquery.datetimepicker.min.css" rel="stylesheet" />
                <link href="${staticPath}/css/ekko-lightbox.css" rel="stylesheet" />
                <!--<link href="${staticPath}/vendor/datetimepicker/jquery.datetimepicker.min.css" rel="stylesheet" />-->
                <link href="${staticPath}/css/ng-strap.css" rel="stylesheet" />

                <!-- <link href="${staticPath}/css/ng-strap-additions.css" rel="stylesheet" /> -->
                <link href="${staticPath}/css/modern-business.css" rel="stylesheet" />
                <link href="${staticPath}/etx-assets/${webapp.id}.css" rel="stylesheet" />

                <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" 
                    rel="stylesheet" 
                    async
                    integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" 
                    crossorigin="anonymous"  />

                ${pageHEAD}

                <script src="${staticPath}/vendor/jquery/jquery.min.js"></script>
                <script src="${staticPath}/js/angular.min.js"></script>
                <script src="${staticPath}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

                <script src="${staticPath}/socket.io/socket.io.js"></script>
                <script src="${staticPath}/vendor/typeahead.bundle.js"></script>
                <!--<script src="${staticPath}/js/moment.js"></script>-->
                <script src="${staticPath}/js/moment-with-locale.min.js"></script>
                <script src="${staticPath}/js/locale.js" charset="UTF-8"></script>
                <script src="${staticPath}/js/underscore.js"></script>
            </head>
            `.trim();

            var sections_html = _.sortBy(sections, function (S) {
                return parseInt(S.fields[psFields.order]||0);
            })
            .map(function (S) {
                var section_name=S.fields[psFields.name];
                var backgroundColor=S.fields[psFields.background_color];
                // var colsize=S.fields[psFields.colsize];
                var colsize='';
                var section_container_sz=S.fields[psFields.container_size];
                var style_code='';

                if (backgroundColor) {
                    style_code+= 'background-color:'+backgroundColor+';';
                }

                var comp_html = comp_list.map(function (C){
                    var comp_type=C.fields[pcFields.type];
                    var comp_name=C.fields[pcFields.name];
                    var colsize=C.fields[pcFields.colsize];
                    var data_policy=C.fields[pcFields.data_policy];
                    var section_id=C.fields[pcFields.section_id];

                    if (section_id=='GLOBAL_SECTION'||section_id!=S.id) {
                        return '';
                    }

                    if (data_policy=='FETCH_LIST') {
                        // colsize='';
                        return C.compiled_html;
                    } else {
                        return '<section comptype="'+comp_type+'" compid="'+C.id+'" name="'+comp_name+'" class="'+colsize + ' comp-cont-'+C.id+'">'+
                                    C.compiled_html+
                                '</section>';
                    }
                }).join("");

                return '<div class=" '+section_container_sz+' ">'+
                            "<div class=\"row rs-margin "+colsize+" \" data-sec-name=\""+section_name+"\" "+
                             " id=\"section-"+S.id+"\" style=\""+style_code+"\">"+
                                comp_html+
                            '</div>'+
                        "</div>";
            })
            .join("");

            var navbar_comp_html=(navbar_comp?navbar_comp.compiled_html:'');
            var footer_comp_html=(footer_comp?footer_comp.compiled_html:'');
            var carrs_comp_html=(carrs_comp?carrs_comp.compiled_html:'');

            // var SB_CLIENT_ID='AbqFLU3lgWTT9BJZnAkfOGY_Q92Nu3FKmrKPDSr1saPhEDcZHaOsMrlN-1WRpR8HVh7U-j_MVTyU05j9';
            // var paymentPageScript='';
            // var pageHasPayment=page.fields[wapFields.has_payment];
            // if (pageHasPayment=="1") {
            //     var clientId=page.fields[wapFields.paypal_client_id];
            //     paymentPageScript='<script src="https://www.paypal.com/sdk/js?currency=EUR&amp;client-id='+clientId+'"></script>'
            // }

            // var loginModal='';
            var loginCompiled='';
            var signupCompiled='';

            var zobject_link="js/zobject-online.js";
            if (modsConf.APP_MODE()!="online") {
                zobject_link="js/zobject-stage.js";
            }

            var body_html = `
                <body style="${pageStyle}" class="${env_label} etx-page-${page.id}">
                    ${navbar_comp_html}

                    ${carrs_comp_html}

                    ${sections_html}

                    ${footer_comp_html}

                    ${loginCompiled}
                    ${signupCompiled}

                    <div style="display:none"
                        ng-controller="AuthController"
                        ng-init="connectWsSocket()">
                    </div>


                    <script src="${staticPath}/vendor/bloodhound.js"></script>

                    <!--<script src="${staticPath}/vendor/datetimepicker/jquery.datetimepicker.full.min.js"></script>-->
                    <script src="${staticPath}/js/jquery.datetimepicker.full.min.js"></script>
                    <script src="${staticPath}/vendor/ekko-lightbox.min.js"></script>
                    <!--
                    <script src="https://vjs.zencdn.net/7.10.2/video.min.js"></script>
                    <script src="${staticPath}/vendor/jquery.zoom.min.js"></script>
                    -->

                    <script src="${staticPath}/vendor/ng-file-upload.min.js"></script>

                    <script src="${staticPath}/vendor/ng-strap/angular-strap.min.js"></script>
                    <script src="${staticPath}/vendor/ng-strap/angular-strap.tpl.min.js"></script>

                    <script src="${staticPath}/${zobject_link}"></script>
                    <script src="${staticPath}/js/cart-service.js"></script>
                    <script src="${staticPath}/js/etx-service.js"></script>
                    <script src="${staticPath}/js/finchain-service.js"></script>
                    <script src="${staticPath}/js/app.js"></script>

                    <script src="${staticPath}/js/auth.js"></script>
                    <script src="${staticPath}/js/account.js"></script>
                    <script src="${staticPath}/js/recom-app.js"></script>
                    <script src="${staticPath}/js/fchain-app.js"></script>
                    <script src="${staticPath}/js/service-app.js"></script>
                    <script src="${staticPath}/js/search-app.js"></script>
                    <script src="${staticPath}/js/search-app22.js"></script>
                    <script src="${staticPath}/js/cart-app.js"></script>
                    <script src="${staticPath}/js/ecommerce-app.js"></script>
                    <script src="${staticPath}/js/ecommerce-app22.js"></script>

                    ${pageJS}

                    <!--
                    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/js-sha512/0.8.0/sha512.js" async defer></script>
                    -->
                    <script src="https://www.google.com/recaptcha/api.js?render=explicit&onload=recaptchaCallback" 
                        async defer>
                    </script>
                    <script type="text/javascript">
                        window.recaptchaCallback = function () {
                            console.info("recaptchaCallback()");
                            window.recaptcha_is_load=true;
                        }
                        $(document).ready(function () {
                            $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                                event.preventDefault();
                                $(this).ekkoLightbox({
                                    leftArrow: '<span class="etx-left-slide fa fa-arrow-circle-left"></span>',
                                    rightArrow: '<span class="etx-right-slide  fa fa-arrow-circle-right"></span>',
                                });
                            });
                        });
                    </script>
                </body>
            `;

            var html = `
                <!DOCTYPE html>
                <html charset="utf-8">
                    ${head_html}
                    ${body_html}
                </html>
            `;

            resolve(html);

        }, function (Err) {
            console.info("[ERROR] on generateComp: ", Err);
            reject({errors: [Err]});
        });
    });
}

//
// COMPONENT INFRAESTRUCTURE v1
//

function generateNavbarSupport(app, page, comp, Locale, env) {
    // /home/ayrton/env/git/99app/cloud-frontend-js/offline-docs/mgcrea-angular-strap-957bbf9/src
    var appRoot = process.env.LAMBDA_TASK_ROOT;
    return;
    if (!support_cache[comp.id]) {
        console.info("[INFO] generating navbar-support for NAVBAR[%s]",
            comp.id
        );
        support_cache[comp.id]='LOADING';
    } else if (support_cache[comp.id]=='LOADING') {
        console.info("[INFO] dup request to navbar-support for NAVBAR[%s] QUEUED!!",
            comp.id
        );
        return;
    } else if (support_cache[comp.id]=='DONE') {
        console.info("[INFO] request to navbar-support for NAVBAR[%s] is DONE!!",
            comp.id
        );
        return;
    }

    var env_path = (!env||env=='PROD')? "" : "/"+env;
    var env_label = ((env=='PROD')? 'production' : env).toLowerCase();

    var sandboxPath=modsConf.c.WEBAPP_SANDBOX_PATH();
    var pageContainer=sandboxPath+"/"+app.id+env_path;
    var page_file=pageContainer+"/navbar-support-"+comp.id.slice(0, 8)+".html";
    // var staticPath=modsConf.c.STATIC_PATH();
    var staticPath=modsConf.c.PUBLICA_HOST_CDN();

    var tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-navbar/navbar-support.html", "utf8");

    return new Promise(function (resolve, reject) {    
        try {
            var code=_.template(tpl);
            var compiled=code({
                $PCF: pcFields,
                $ECF: ecFields,
                $ENF: entFields,
                $WAF: waFields,
                $WAPF: wapFields,
                $SVF: svFields,
                $BXF: bxFields,
                $INV: invtFields,
                $OTF: otFields,
                $PME: pmeFields,
                $BLF: blFields,
                $MCF: mcFields,
                moment: moment,
                moneyFmt: wbUtils.moneyFormater(),
                WS_HOST: modsConf.c.WsHost(),
                STATIC_PATH: staticPath,
                compobj: comp,
                TR: Locale.TR,
                webpage: page,
                webapp: app
            });

            resolve(compiled);
            // writeToFile(page_file, compiled)
            // .then(function (resp) {
            //     support_cache[comp.id]='DONE';
            //     setTimeout(function () {
            //         support_cache[comp.id]=false;
            //     }, 6000);
            //     console.info("generated navbar-support:", page_file);
            // }, function (Err) {
            //     support_cache[comp.id]=false;
            //     console.info("could not generate navbar-support:", Err);
            //     reject({errors: [Err]});
            // });
        } catch (e) {
            support_cache[comp.id]=false;
            console.error("[ERROR] Could not compile navbar-support for [%s:%s]", 
                comp.fields[pcFields.name], 
                comp.fields[pcFields.type]
            );
            console.error("[ERROR] More details ", e);
            // console.error("[ERROR] Template %s", tpl);
            reject({
                // comp:comp, 
                errors: [e.message]
            });
        }
    });
}

function generateComp(Auth, orgObj, webapp, page, comp, page_list, sections, Locale, env) {
    var comp_type=comp.fields[pcFields.type];
    var data_policy=comp.fields[pcFields.data_policy];
    var tpl, EXT={};
    var compPromise=Promise.resolve();
    var staticPath=modsConf.c.PUBLICA_HOST_CDN();
    // var staticPath=modsConf.c.STATIC_PATH();
    
    // var appRoot=process.env.LAMBDA_TASK_ROOT;
    var appRoot=modsConf.c.PROJECT_HOME();

    if (comp_type=="SITE_NAVBAR") {
        var confObj=JSON.parse(comp.fields[pcFields.conf_obj]||'{}');
        comp.conf_obj_str = JSON.stringify(comp.fields[pcFields.conf_obj]);
        comp.conf_obj = (comp.fields[pcFields.conf_obj]);
        comp.confObjStr=(comp.fields[pcFields.conf_obj]);
        comp.confObj=confObj;

        if (comp.confObj.menu) {
            comp.confObj.menu.forEach(function (menu) {
                if (!menu.id) {
                    menu.id=menu.label+"-"+menu.link;
                }
            });
        }

        // console.info("=========> SITE_NAVBAR[conf]: ", comp.confObj);

        var image=comp.fields[pcFields.image];
        comp.navbar_title = comp.fields[pcFields.title_df];
        comp.has_logo=false;

        if (image) {
            // comp.navbar_title = '';
            comp.has_logo=true;
        }

        if (!confObj.type||confObj.type=='DEFAULT') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-navbar/navbar.html", "utf8");

        } else if (confObj.type=='DEFAULT_SEARCH') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-navbar/app-menu-search.html", "utf8");
            generateNavbarSupport(webapp, page, comp, Locale, env);

        } else if (confObj.type=='ECOMMERCE_SIMPLE') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-navbar/ecommerce-simple.html", "utf8");
            generateNavbarSupport(webapp, page, comp, Locale, env);

        } else if (confObj.type=='ECOMMERCE_SIMPLE_V3') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-navbar/ecommerce-simple-v3.html", "utf8");
            generateNavbarSupport(webapp, page, comp, Locale, env);

        } else if (confObj.type=='ECOMMERCE_SIMPLE_V4') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-navbar/ecommerce-simple-v4.html", "utf8");
            generateNavbarSupport(webapp, page, comp, Locale, env);

        } else if (confObj.type=='ECOMMERCE_SIMPLE_V2') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-navbar/ecommerce-simple-v2.html", "utf8");
            generateNavbarSupport(webapp, page, comp, Locale, env);

        } else if (confObj.type=='ECOMMERCE_SIMPLE_SUBMENU') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-navbar/ecommerce-simple-submenu.html", "utf8");
            generateNavbarSupport(webapp, page, comp, Locale, env);

        } else if (confObj.type=='VSTREAM_CLOUD_v1') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-navbar/vstream-cloud-v1.html", "utf8");
            generateNavbarSupport(webapp, page, comp, Locale, env);

        } else if (confObj.type=='VSTREAM_CLOUD_v2') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-navbar/vstream-cloud-v2.html", "utf8");
            generateNavbarSupport(webapp, page, comp, Locale, env);

        } else if (confObj.type=='ETX_SUPPORT') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-navbar/etx-support.html", "utf8");
            generateNavbarSupport(webapp, page, comp, Locale, env);

        } else if (confObj.type=='REALSTATE_v1') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-navbar/realstate-v1.html", "utf8");
            generateNavbarSupport(webapp, page, comp, Locale, env);
        } else if (confObj.type=='REALSTATE_v2') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-navbar/realstate-v2.html", "utf8");
            generateNavbarSupport(webapp, page, comp, Locale, env);

        } else {
            // tpl = '';
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-navbar/navbar.html", "utf8");
        }

    } else if (comp_type=="SITE_FOOTER") {
        var confObj=JSON.parse(comp.fields[pcFields.conf_obj]||'{}');
        comp.confObj=confObj;

        var file=appRoot + "/webapp/templates/wb-comp/wb-footer/footer.html";
        if (confObj.template=="ECOMMERCE_V2") {
            file=appRoot + "/webapp/templates/wb-comp/wb-footer/footer-ecommercev2.html"
        } else if (confObj.template=="REALSTATE_v1") {
            file=appRoot + "/webapp/templates/wb-comp/wb-footer/footer-realstate.html"
        } else if (confObj.template=="KUALEH_FOOTER") {
            file=appRoot + "/webapp/templates/wb-comp/wb-footer/footer-kualeh.html"
        }

        tpl=fs.readFileSync(file, "utf8");

    } else if (comp_type=="APP_SIGNUP") {
        var confObj=JSON.parse(comp.fields[pcFields.conf_obj]||'{}');
        comp.confObj=confObj;
        var tpl_path=appRoot + "/webapp/templates/wb-comp/wb-security/signup.html";
        tpl=fs.readFileSync(tpl_path, "utf8");

    } else if (comp_type=="APP_SIGNIN") {
        var confObj=JSON.parse(comp.fields[pcFields.conf_obj]||'{}');
        comp.confObj=confObj;
        var tpl_path=appRoot + "/webapp/templates/wb-comp/wb-security/signin.html";
        tpl=fs.readFileSync(tpl_path, "utf8");

    } else if (comp_type=="APP_ACCOUNT") {
        var confObj=JSON.parse(comp.fields[pcFields.conf_obj]||'{}');;
        comp.confObj=confObj;
        var tpl_file;
        var purchase_mod='ecomerce-cart-buy.html';
        var ecommece_buy_comp_tpl_file=appRoot + "/webapp/templates/wb-comp/wb-account/";

        if (!confObj.template||confObj.template=="DEFAULT"){
            tpl_file=appRoot + "/webapp/templates/wb-comp/wb-account/my-account-ecom.html";

        } else if (confObj.template=="ETX_SUPPORT") {
            // XXX
            // tpl_file=appRoot + "/webapp/templates/wb-comp/wb-account/my-account-settings.html" ;
            tpl_file=appRoot + "/webapp/templates/wb-comp/wb-account/my-account-profile-v2.html";

        } else if (confObj.template=="RS_SUPPORT") {
            tpl_file=appRoot + "/webapp/templates/wb-comp/wb-account/my-account-realstate.html";            

        } else if (confObj.template=="FCHAIN_COMP") {
            tpl_file=appRoot + "/webapp/templates/wb-comp/wb-account/my-account-finchain.html";            
        
        } else if (confObj.template=="PROFILE_COMP") {
            tpl_file=appRoot + "/webapp/templates/wb-comp/wb-account/my-account-profile.html";           

        } else if (confObj.template=="ETX_PROFILE") {
            tpl_file=appRoot + "/webapp/templates/wb-comp/wb-account/my-account-profile-v2.html";           

        } else if (confObj.template=="ETX_SELLER_PROFILE") {
            tpl_file=appRoot + "/webapp/templates/wb-comp/wb-seller/seller-console.html";           
            ecommece_buy_comp_tpl_file=appRoot + "/webapp/templates/wb-comp/wb-seller/";
            purchase_mod='ecomerce-cart-sell.html';

        } else if (confObj.template=="ETX_FORGOT_PASSWORD") {
            tpl_file=appRoot + "/webapp/templates/wb-comp/wb-account/forgot-password.html";           

        } else if (confObj.template=="ETX_RESET_PASSWORD") {
            tpl_file=appRoot + "/webapp/templates/wb-comp/wb-account/reset-password.html";           

        } else if (confObj.template=="ETX_PURCHASE") {
            tpl_file=appRoot + "/webapp/templates/wb-comp/wb-purchase/my-account-purchase.html";
            ecommece_buy_comp_tpl_file=appRoot + "/webapp/templates/wb-comp/wb-purchase/";
            purchase_mod='ecomerce-cart-buy-v2.html';

        } else if (confObj.template=="ETX_PAYPAL") {
            tpl_file=appRoot + "/webapp/templates/wb-comp/wb-purchase/my-account-purchase-paypal.html";
            ecommece_buy_comp_tpl_file=appRoot + "/webapp/templates/wb-comp/wb-purchase/";
            purchase_mod='ecomerce-cart-buy-paypal.html';

        } else if (confObj.template=="ETX_ECOM_ACCOUNT") {
            tpl_file=appRoot + "/webapp/templates/wb-comp/wb-purchase/my-ecommerce-account.html";
            ecommece_buy_comp_tpl_file=appRoot + "/webapp/templates/wb-comp/wb-purchase/";
            purchase_mod='ecomerce-cart-buy-v2.html';
        }

        var compiled22='';

        if (!support_cache[comp.fields[pcFields.page_id]]) {
            setTimeout(function () {
                support_cache[comp.fields[pcFields.page_id]]=false;
            }, 5000);

            support_cache[comp.fields[pcFields.page_id]]=true;
            var shopping_cart_modals_html=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-ecommerce/shopping-cart-modals.html", "utf8");
            var code22=_.template(shopping_cart_modals_html);
            compiled22=code22({
                compObj: comp,
                $INV: invtFields,
                $PCF: pcFields,
                $ECF: ecFields,
                $PME: pmeFields,
                $BLF: blFields,
                $MCF: mcFields,
                compobj: comp,
                TR: Locale.TR,
                moneyFmt: wbUtils.moneyFormater(),
            });
        }

        comp.shopping_cart_html = compiled22;

        tpl=fs.readFileSync(tpl_file, "utf8");

        var ecommece_buy_comp=fs.readFileSync(ecommece_buy_comp_tpl_file+purchase_mod, "utf8");
        var payment_conf_obj;

        try {
            var payment_conf=comp.fields[pcFields.payment_conf];
            if (payment_conf) {
                payment_conf_obj=JSON.parse(payment_conf||'{list:[]}');
            } else {
                payment_conf_obj={
                    list:[]
                }                
            }

        } catch (e) {
            console.error("FAILED TO PARSE PAYMENT CONF: ", e);
            payment_conf_obj={
                list:[]
            }
        }

        comp.payment_conf_obj=payment_conf_obj;

        tpl=tpl.replace("__ecomerceCartBuy__", ecommece_buy_comp);
        // var api=new APIService("object", Auth);
        // if (confObj.cart_comp_id) {
        //     compPromise = api.get(confObj.cart_comp_id)
        //                     .then(function (Resp) {
        //                         Resp.cart_conf = JSON.parse(Resp.fields[pcFields.conf_obj]||'{}');
        //                         return Resp;
        //                     });
        // }

    } else if (comp_type=="APP_VHTML_COMPONENT") {
        var confObj=JSON.parse(comp.fields[pcFields.conf_obj]||'{}');;
        comp.confObj=confObj;

        var sandboxPath=modsConf.c.WEBAPP_SANDBOX_PATH();
        // appRoot + "/webapp/templates/wb-comp/wb-vhtml/comp/"+confObj.script_name+".html";
        var tpl_path;
        tpl_path=sandboxPath + "/etx-assets/"+webapp.id+"/"+comp.id+".vhtml";

        try {
            var payment_conf=comp.fields[pcFields.payment_conf];
            if (payment_conf) {
                payment_conf_obj=JSON.parse(payment_conf||'{list:[]}');
            } else {
                payment_conf_obj={
                    list:[]
                }                
            }

        } catch (e) {
            console.error("FAILED TO PARSE PAYMENT CONF: ", e);
            payment_conf_obj={
                list:[]
            }
        }

        comp.payment_conf_obj=payment_conf_obj;

        try {
            if (!fs.existsSync(tpl_path)) {
                console.info('[WARN] "%s" doest not exist!', tpl_path);
                tpl_path=appRoot + "/webapp/templates/wb-comp/wb-vhtml/comp/default.html";
            }

        } catch(err) {
            console.error("[ERROR] Could not verify file: %s", tpl_path);
            console.error("[ERROR] Details %o", err);
            tpl_path=appRoot + "/webapp/templates/wb-comp/wb-vhtml/comp/default.html";
        }

        tpl=fs.readFileSync(tpl_path, "utf8");

    } else if (comp_type=="APP_PRODUCTGROUP") {
        var enable_compr=comp.fields[pcFields.enable_compr];
        if (enable_compr) {
            var compOptm = wbUtils.ProcessCompOptmizerConf(comp, {image: comp.fields[pcFields.image]});
            comp.OPTM=compOptm;            
        } else {
            comp.OPTM=false;            
        }

        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-productgroup/productgroup.html", "utf8");
        comp.conf_obj = (comp.fields[pcFields.conf_obj]);
        var confObj=JSON.parse(comp.fields[pcFields.conf_obj]||'{}');;
        comp.confObj=confObj;

        // if (confObj.render_template=='DESTAQUE') {
        //     tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-productgroup/product-destaque.html", "utf8");
        // } else if (confObj.render_template=='DESTAQUE_V2') {
        //     tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-productgroup/product-destaque-V2.html", "utf8");
        // }

        // var item_html=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-productgroup/cartlist-item.tpl.html", "utf8");
        // if (confObj.render_template=='DESTAQUE') {
        //     item_html=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-productgroup/product-item.html", "utf8");
        // } else if (confObj.render_template=='DESTAQUE_V2') {
        //     item_html=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-productgroup/V2/product-item.html", "utf8");
        // }

        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-productgroup/product-destaque-V2.html", "utf8");
        item_html=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-productgroup/V2/product-item.html", "utf8");

        var catCache={};
        var code=_.template(item_html);

        var compiled=code({
            compObj: comp,
            $INV: invtFields,
            $PCF: pcFields,
            $ECF: ecFields,
            $PME: pmeFields,
            $BLF: blFields,
            compobj: comp,
            item: {},
            TR: Locale.TR,
            moneyFmt: wbUtils.moneyFormater(),
        });

        comp.item_html=compiled;
        comp.category_list=Object.keys(catCache).map(function (C) {
            return catCache[C];
        });

    } else if (comp_type=="SITE_CARROUSEL") {
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-carrs/carrs.html", "utf8");
        comp.conf_obj = (comp.fields[pcFields.conf_obj]);
        comp.confObj = JSON.parse(comp.fields[pcFields.conf_obj]||'{"items":[]}');
        comp.img_href="{{obj.img}}";
        comp.indexC="{{$index}}";

        if (!comp.confObj.items) {
            comp.confObj.items=[];
        }

        comp.confObj.items.forEach(function (xItem) {
            console.info("CARR-Item[compressor] ", xItem.compressor);
            if (xItem.compressor) {
                var Optm = wbUtils.ProcessCompressorConf(xItem.compressor, {image: xItem.img});
                if (Optm && Optm.gen) {
                    xItem.img = Optm.gen;
                    xItem.r_sz= Optm.sz;
                }
            }
        });

    } else if (comp_type=="APP_BANNER_CAR") {
        var tpl_mod=appRoot + "/webapp/templates/wb-comp/wb-banner-car/banner-car.html";
        var confObj = JSON.parse(comp.fields[pcFields.conf_obj]||'{}');
        comp.confObj = confObj;

        confObj.items.forEach(function (xItem) {
            // console.info("xItem[compressor] ", xItem.compressor);
            xItem.original_image=xItem.img;
            if (xItem.compressor) {
                var Optm = wbUtils.ProcessCompressorConf(xItem.compressor, {image: xItem.img});
                console.info("ProcessCompOptmizerConf() res: ", Optm);
                if (Optm && Optm.gen) {
                    xItem.img = Optm.gen;
                    xItem.r_sz= Optm.sz;
                }
            }
        });

        var img_engine=confObj.img_engine;
        var template=confObj.template||'DEFAULT';

        if (confObj.template=='DEFAULT') {
            tpl_mod=appRoot + "/webapp/templates/wb-comp/wb-banner-car/banner-car.html";
            if (img_engine=="TAG_IMG") {
                tpl_mod=appRoot + "/webapp/templates/wb-comp/wb-banner-car/banner-car-img.html";
            }

        } else if(confObj.template=='GALLERY') {
            tpl_mod=appRoot + "/webapp/templates/wb-comp/wb-banner-car/banner-gallery.html";

        } else if(confObj.template=='GRID') {
            tpl_mod=appRoot + "/webapp/templates/wb-comp/wb-banner-car/banner-grid.html";
        
        } else if(confObj.template=='SLIDER') {
            if (img_engine=="TAG_IMG") {
                tpl_mod=appRoot + "/webapp/templates/wb-comp/wb-banner-car/banner-slider-img.html";
            } else {
                tpl_mod=appRoot + "/webapp/templates/wb-comp/wb-banner-car/banner-slider-bk.html";
            }
        }

        tpl=fs.readFileSync(tpl_mod, "utf8");
        comp.conf_obj = (comp.fields[pcFields.conf_obj]);

        comp.img_href="{{obj.img}}";
        comp.indexC="{{$index}}";

    } else if (comp_type=="APP_TEXT_INTRO") {
        // XxXxXxXx
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-textintro/textintro.html", "utf8");

    } else if (comp_type=="APP_SIMPLE_TEXTBOX") {
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-textbox/textbox.html", "utf8");
        comp.readmore_link=comp.fields[pcFields.readmore_link];

    } else if (comp_type=="APP_SIMPLE_CARD1"||
              comp_type=="APP_SIMPLE_CARD2"||
              comp_type=="APP_SIMPLE_CARD3"||
              comp_type=="APP_SIMPLE_CARD4") {

        var subtype=comp.fields[pcFields.data_policy];

        var card_tpl=appRoot + "/webapp/templates/wb-comp/wb-scard/"+comp_type+"/main.html";

        if (subtype=='SUPERCARD' && comp_type!='APP_SIMPLE_CARD1') {
            card_tpl=appRoot + "/webapp/templates/wb-comp/wb-scard/"+comp_type+"/supercard.html";
        }

        var enable_compr=comp.fields[pcFields.enable_compr];

        console.info("comp.fields[pcFields.conf_obj]: ", comp.fields[pcFields.conf_obj]);

        comp.confObj = JSON.parse(comp.fields[pcFields.conf_obj]||'{"items":{}}');

        tpl=fs.readFileSync(card_tpl, "utf8");


    } else if (comp_type=="APP_SIMPLE_CARD") {
        var card_tpl=appRoot + "/webapp/templates/wb-comp/wb-scard/scard.html";
        var cardTplConf=comp.fields[pcFields.conf_obj_id];
        var enable_compr=comp.fields[pcFields.enable_compr];
        if (enable_compr) {
            var compOptm = wbUtils.ProcessCompOptmizerConf(comp, {image: comp.fields[pcFields.image]});
            comp.OPTM=compOptm;            
        } else {
            comp.OPTM=false;            
        }

        if (cardTplConf=="SUPER_CARD") {
            card_tpl=appRoot + "/webapp/templates/wb-comp/wb-scard/super-card.html";
            var markdown=comp.fields[pcFields.descr_df];
            try {
                var compiled='';
                if (markdown&&markdown.length>0) {                
                    compiled=wbUtils.compileCompMarkdown(comp, markdown);
                    comp.compiled_html=compiled;
                }
            } catch (e) {
                comp.compiled_html='';
                // console.error("[ERROR] Could not compile markdown for component[APP_SIMPLE_CARD, %s]",
                //     comp.id,
                // );
                // console.error("[ERROR] markdown-content '%s'", markdown);
            }
        }

        comp.card_conf = JSON.parse(comp.fields[pcFields.conf_obj]||'{}');
        tpl=fs.readFileSync(card_tpl, "utf8");

    } else if (comp_type=="APP_SERVICE_CMS" || comp_type=="APP_SERVICE_ITEM") {
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-service-cms/service-list.html", "utf8");
        comp.service_item_conf = JSON.parse(comp.fields[pcFields.conf_obj]||'{}');
        // if (data_policy=="FETCH_LIST") {
        //     // console.info("DynamicComponent: ", comp);
        //     tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-svcitem/svcitem-flist.html", "utf8");
        //     if (comp.fields[pcFields.image]) {
        //         comp.fields[pcFields.image]="{{"+comp.fields[pcFields.image]+"}}";
        //     } else {
        //         comp.fields[pcFields.image]="/img/bgray.png";
        //     }
        // } else {
        //     tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-svcitem/svcitem.html", "utf8");
        // }

    } else if (comp_type=="APP_DONATION_CARD") {
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-donate/donate.html", "utf8");
        var totalAmount=parseFloat(comp.fields[pcFields.amount]||0);
        var progress=parseFloat(comp.fields[pcFields.progress]||0);
        if (progress!=0) {
            EXT.percent_progress=parseInt((progress/Math.max(totalAmount, 1)) * 100);
        } else {
            EXT.percent_progress=0;
        }

    } else if (comp_type=="APP_CONTACT_INFO") {
        // xxx
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-contact-info/contactus.html", "utf8");

    } else if (comp_type=="APP_SERVICE_REQ") {
        // xxxx
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-requestsvc/requestsvc.html", "utf8");

    } else if (comp_type=="APP_FINTECH") {
        var conf = JSON.parse(comp.fields[pcFields.conf_obj]||'{}');
        comp.fintech_conf=conf;

        if (conf.ent_type=='AFILIATE_PROGRAM') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-fintech/wb-affiliate.html", "utf8");

        } else if (conf.ent_type=='AFILIATE_ACCOUNT') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-fintech/wb-affiliate-account.html", "utf8");

        } else if (conf.ent_type=='DEFAULT_AUCTION') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-fintech/wb-default-auction.html", "utf8");

        } else if (conf.ent_type=='LIVE_AUCTION') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-fintech/wb-live-auction.html", "utf8");
        }

    } else if (comp_type=="APP_ENTITY") {
        var conf = JSON.parse(comp.fields[pcFields.conf_obj]||'{}');
        comp.entity_system_conf=conf;

        try {
            var payment_conf=comp.fields[pcFields.payment_conf];
            console.info("=====> payment_conf: ", payment_conf)
            if (payment_conf) {
                payment_conf_obj=JSON.parse(payment_conf||'{list:[]}');
            } else {
                payment_conf_obj={
                    list:[]
                }                
            }

        } catch (e) {
            console.error("FAILED TO PARSE PAYMENT CONF: ", e);
            payment_conf_obj={
                list:[]
            }
        }

        comp.payment_conf_obj=payment_conf_obj;

        var enable_compr=comp.fields[pcFields.enable_compr];
        if (enable_compr) {
            var compOptm = wbUtils.ProcessCompOptmizerConf(comp, {image: comp.fields[pcFields.image]});
            comp.OPTM=compOptm;            
        } else {
            comp.OPTM=false;            
        }

        // tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-entity-system/etn-system.html", "utf8");
        if (conf.ent_type=='ALBUM') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-entity-system/album.html", "utf8");

        // XXXXX 
        // REALSTATE
        // XXXXX
        } else if (conf.ent_type=='RSTE_HOUSE') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-realstate/house-main.html", "utf8");

        } else if (conf.ent_type=='RSTE_HOST') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-realstate/host-main.html", "utf8");

        } else if (conf.ent_type=='RSTE_EXPERIENCE') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-realstate/experience-catalog.html", "utf8");

        } else if (conf.ent_type=='RSTE_EXPERIENCE_SEARCH') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-realstate/experience-search.html", "utf8");

        } else if (conf.ent_type=='RSTE_LISTS') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-realstate/lists-catalog.html", "utf8");

        } else if (conf.ent_type=='RSTE_EXPERIENCE_VIEW') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-realstate/experience-main.html", "utf8");

        } else if (conf.ent_type=='RSTE_OBJECT_VIEWER') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-realstate/object-viewer.html", "utf8");

        } else if (conf.ent_type=='RSTE_ATTRACTION') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-realstate/attraction-main.html", "utf8");

        } else if (conf.ent_type=='RSTE_PROPERTY') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-realstate/property-catalog.html", "utf8");

        } else if (conf.ent_type=='RSTE_PROPERTY_VIEWER') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-realstate/property-main.html", "utf8");

        // BLOG SYSTEM
        } else if (conf.ent_type=='BLOG_CMS') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-blog/blog-cms.html", "utf8");
        } else if (conf.ent_type=='VIEW_BLOG_POST') {

            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-blog/view-blog-post.html", "utf8");

        // LIVE SYSTEM
        } else if (conf.ent_type=='LIVE_STREAM_FULL') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-entity-system/live-stream-full.html", "utf8");

            var chatCompTPL=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-entity-system/etx-chat-comp.html", "utf8");
            var chatTipsTPL=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-entity-system/etx-chat-tips.html", "utf8");
            // console.info("chatCompTPL: ", chatCompTPL);

            tpl = tpl.replace("__chatComp__", chatCompTPL);
            tpl = tpl.replace("__tipsComp__", chatTipsTPL);

        } else if (conf.ent_type=='FEED') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-entity-system/etx-feed.html", "utf8");

        } else if (conf.ent_type=='CHAT_SYSTEM') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-entity-system/etx-chat-system.html", "utf8");
            // __liveComp__

        } else if (conf.ent_type=='CHAT_COMPONENT') {
            // tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-entity-system/etx-chat-comp-v2.html", "utf8");
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-entity-system/etx-chat-comp.html", "utf8");

        } else if (conf.ent_type=='APP_PROFILE') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-entity-system/etx-user-profile.html", "utf8");

        } else if (conf.ent_type=='AFILIATE_PROGRAM') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-fintech/wb-dashboard.html", "utf8");

        // VIDEO-SYSTEM
        } else if (conf.ent_type=='VIDEO_SYSTEM') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-entity-system/video-system.html", "utf8");

        } else if (conf.ent_type=='CHANNEL_VIDEO_SYSTEM') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-entity-system/channel-video-system.html", "utf8");

        } else if (conf.ent_type=='CHANNEL_VIDEO_SYSTEM_TEST') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-entity-system/channel-video-system-test.html", "utf8");

        } else if (conf.ent_type=='VIDEO_CATALOG') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-vstream/video-catalog.html", "utf8");

        } else if (conf.ent_type=='VIDEO_SEARCH') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-vstream/video-search.html", "utf8");

        } else if (conf.ent_type=='VIDEO_MWATCHER') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-vstream/video-mwatcher.html", "utf8");

        } else if (conf.ent_type=='VIDEO_FWATCHER') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-vstream/video-fwatcher.html", "utf8");

        } else if (conf.ent_type=='FEED_ADMIN') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-entity-system/etx-feed-admin.html", "utf8");

        } else if (conf.ent_type=='SONG') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-entity-system/song-submission.html", "utf8");

        } else if (conf.ent_type=='GAMMING_PLATFORM') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-gamming/main.html", "utf8");

        }

        // tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-entity-system/etn-system.html", "utf8");
        if (tpl) {
            var feedbackTPL=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-entity-system/feedback-v2.html", "utf8");
            tpl = tpl.replace("__feedbackv2__", feedbackTPL);

        }

    } else if (comp_type=="APP_ZSTORAGE_OBJ") {
        var zstorage_conf=JSON.parse(comp.fields[pcFields.conf_obj]||'{}');
        comp.zstorage_conf = zstorage_conf;
        var subtype=comp.fields[pcFields.data_policy];
        // 'Z_LISTING'
        // 'Z_CATALOG'
        // 'Z_VIEWER'
        // 'Z_SEARCH'
        // 'Z_RECOMMEND'
        // 'Z_HISTORY'
        // 'Z_CUSTOM1'
        // 'Z_CUSTOM2'
        // 'Z_CUSTOM3'

        var module=subtype.toLowerCase()+".html";
        var template=zstorage_conf.template;

        if (template=="RST_HOST") {
            tpl_file=appRoot + "/webapp/templates/wb-comp/wb-zstorage/templates/rst_host.html";

        } else if (template=="RST_HOST_VIEW") {
            tpl_file=appRoot + "/webapp/templates/wb-comp/wb-zstorage/templates/rst_host_view.html";

        } else if (template=="RST_HOUSE") {
            tpl_file=appRoot + "/webapp/templates/wb-comp/wb-zstorage/templates/rst_house.html";

        } else if (template=="RST_HOUSE_VIEW") {
            tpl_file=appRoot + "/webapp/templates/wb-comp/wb-zstorage/templates/rst_house_view.html";
    
        } else {
            tpl_file=appRoot + "/webapp/templates/wb-comp/wb-zstorage/zobj/"+module;
        }
        
        tpl=fs.readFileSync(tpl_file, "utf8");

        var xtpl_file=appRoot + "/webapp/templates/wb-comp/wb-zstorage/templates/rst_host_reservation_form.html"
        var plugin_html=fs.readFileSync(xtpl_file, "utf8");
        tpl=tpl.replace('__reservationPlugin__', plugin_html);

    } else if (comp_type=="APP_ZSTORAGE_PROP") {
        var conf=JSON.parse(comp.fields[pcFields.conf_obj]||'{}');
        comp.zstorage_conf = conf;
        var subtype=comp.fields[pcFields.data_policy];
        // 'Z_LISTING'
        // 'Z_CATALOG'
        // 'Z_VIEWER'
        // 'Z_SEARCH'
        // 'Z_RECOMMEND'
        // 'Z_HISTORY'
        // 'Z_CUSTOM1'
        // 'Z_CUSTOM2'
        // 'Z_CUSTOM3'

        comp.entity_item_conf = (comp.fields[pcFields.conf_obj]);

        var module=subtype.toLowerCase()+".html";
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-zstorage/zprop/"+module, "utf8");

    } else if (comp_type=="APP_ENTITY_ITEM") {
        var conf=(comp.entity_item_conf = JSON.parse(comp.fields[pcFields.conf_obj]||'{}'));
        var subtype=comp.fields[pcFields.data_policy];
        var ent_tpl;

        var enable_compr=comp.fields[pcFields.enable_compr];
        if (enable_compr) {
            var compOptm = wbUtils.ProcessCompOptmizerConf(comp, {image: comp.fields[pcFields.image]});
            comp.OPTM=compOptm;            
        } else {
            comp.OPTM=false;            
        }

        if (subtype=='BLOG_POST') {
            if (conf.enable_header) {
                ent_tpl='entity-blogpost.html';
            } else {
                ent_tpl='entity-blogpost-simple.html';
            }

        } else if (subtype=='SERV_POST') {
            ent_tpl='entity-service.html';

        } else if (subtype=='PROD_POST') {
            ent_tpl='entity-service.html';

        } else if (subtype=='REVIEW_PLUGIN') {
            ent_tpl='review-plugin.html';

        } else {
            ent_tpl="df-item.html";
        }

        comp.entity_item_conf = (comp.fields[pcFields.conf_obj]);
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-entity-item/"+ent_tpl, "utf8");

        if ((subtype=='BLOG_POST') && conf && conf.post_id) {
            var api=new APIService("object", Auth);
            compPromise = api.get(conf.post_id)
            .then(function (postObj) {
                var markdown=postObj.fields[bxFields.content_df];
                var html_compiled=wbUtils.compileCompMarkdown(comp, markdown);
                tpl=tpl.replace('___post_html____', html_compiled);
                return postObj;
            });
        }

    } else if (comp_type=="APP_CHECKOUT") {
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-checkout/checkout.html", "utf8");

    } else if (comp_type=="APP_CALENDAR") {
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-calendar/calendar.html", "utf8");

    } else if (comp_type=="APP_BOOKING") {
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-booking/booking.html", "utf8");

    } else if (comp_type=="APP_AGENDA") {
        comp.agenda_conf = JSON.parse(comp.fields[pcFields.conf_obj]||'{}');
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-agenda/component.html", "utf8");

    } else if (comp_type=="APP_AGENDA_CARD") {
        comp.confObj = JSON.parse(comp.fields[pcFields.conf_obj] ||'{"items":{}}');

        if (!comp.confObj.items) {
            comp.confObj.items=[];
        }
        
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-agenda-card/component.html", "utf8");

    } else if (comp_type=="APP_STORIES") {
        comp.agenda_conf = JSON.parse(comp.fields[pcFields.conf_obj]||'{}');
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-stories/component.html", "utf8");

    } else if (comp_type=="APP_VIDEO") {
        var video_conf = JSON.parse(comp.fields[pcFields.conf_obj]||'{"items:" {}}');
        comp.video_conf = video_conf;
        comp.FormatDuration = function (secs) {
            var d=moment.duration(secs, "seconds");
            var ut=moment.utc(d.as('milliseconds'));
            if (parseInt(ut.format("HH"))==0) {
                return ut.format("mm:ss");
            } else {
                return ut.format("HH:mm:ss");
            }
        }

        if (!comp.video_conf.items) {
            comp.video_conf.items=[];
        }

        console.info("video_conf.template: ", video_conf.template);

        if (video_conf.template=='YOUTUBE') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-video/templates/youtube-tpl.html", "utf8");

        } else {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-video/component.html", "utf8");
        }

    } else if (comp_type=="APP_VIDEO_GRID") {
        comp.agenda_conf = JSON.parse(comp.fields[pcFields.conf_obj]||'{}');
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-video/grid.html", "utf8");

    } else if (comp_type=="APP_SPLASH_CARRS") {
        comp.agenda_conf = JSON.parse(comp.fields[pcFields.conf_obj]||'{}');
        comp.confObj=comp.agenda_conf;
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-splash-carrs/component.html", "utf8");

    } else if (comp_type=="APP_SPLASH_PRODUCT") {
        var enable_compr=comp.fields[pcFields.enable_compr];
        if (enable_compr) {
            var compOptm = wbUtils.ProcessCompOptmizerConf(comp, {image: comp.fields[pcFields.image]});
            comp.OPTM=compOptm;            
        } else {
            comp.OPTM=false;            
        }

        var subtype=comp.fields[pcFields.data_policy];
        comp.banner_conf = JSON.parse(comp.fields[pcFields.conf_obj]||'{}');
        comp.prod_conf = JSON.parse(comp.fields[pcFields.conf_obj]||'{}');
        if (subtype=='BANNER_PLUGIN') {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-splash-product/banner-product.html", "utf8");
        } else {
            tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-splash-product/splash-product.html", "utf8");
        }

    } else if (comp_type=="APP_SERVICE_VIEW") {
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-service-cms/service-view.html", "utf8");
        comp.service_view_conf = JSON.parse(comp.fields[pcFields.conf_obj]||'{}');

    } else if (comp_type=="APP_SERVICE_DETAILS") {
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-service-details/service-details.html", "utf8");
        comp.service_details_conf = JSON.parse(comp.fields[pcFields.conf_obj]||'{}');

    } else if (comp_type=="APP_RECOMMENDER") {
        // var enable_compr=comp.fields[pcFields.enable_compr];
        // if (enable_compr) {
        //     var compOptm = wbUtils.ProcessCompOptmizerConf(comp, {image: comp.fields[pcFields.image]});
        //     comp.OPTM=compOptm;            
        // } else {
        //     comp.OPTM=false;            
        // }

        var subtype=comp.fields[pcFields.data_policy];
        comp.recom_conf = JSON.parse(comp.fields[pcFields.conf_obj]||'{}');
        comp.cat_list = JSON.parse(comp.fields[pcFields.category]||'{}');

        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-recommender/default-recommender.html", "utf8");

    } else if (comp_type=="APP_ECOMMERCE") {
        var enable_compr=comp.fields[pcFields.enable_compr];
        if (enable_compr) {
            var compOptm = wbUtils.ProcessCompOptmizerConf(comp, {image: comp.fields[pcFields.image]});
            comp.OPTM=compOptm;            
        } else {
            comp.OPTM=false;            
        }

        var subtype=comp.fields[pcFields.data_policy];
        comp.ecommerce_conf = JSON.parse(comp.fields[pcFields.conf_obj]||'{}');
        comp.category_conf_str = comp.fields[pcFields.category];
        comp.category_conf = JSON.parse(comp.fields[pcFields.category]||'{}');

        var item_template="default.html";
        if (comp.ecommerce_conf.prod_template=='SMALL-2') {
            item_template="compact-2.html";
        }

        var module;

        if (subtype=='STATIC_DATA_POLICY') {
            module="ecommerce-page.html";
            if (comp.ecommerce_conf.search_engine=="ETX_SEARCH") {
                module="ecommerce-page-v2.html";
                item_template="default-v2.html";
            } else if (comp.ecommerce_conf.search_engine=="ETX_RECOMMENDER") {
                module="ecommerce-recom.html";
                item_template="recom-item.html";
            } 
            else if (comp.ecommerce_conf.search_engine=="HORIZONTAL_LIST") {
                module="ecommerce-horizontal-list.html";
                item_template="default-v2.html";
            }
            else if (comp.ecommerce_conf.search_engine=="LIST_DESTAQUE") {
                module="ecommerce-destaque-list.html";
                item_template="default-v2.html";
            }
            else if (comp.ecommerce_conf.search_engine=="MARKETPLACE") {
                module="ecommerce-marketplace.html";
                item_template="default-v2.html";
            }
            else if (comp.ecommerce_conf.search_engine=="LIST_GRID") {
                module="ecommerce-listing-grid.html";
                item_template="default-v2.html";
            }

        } else if (subtype=='SEARCH_PLUGIN') {
            module="ecommerce-search.html";
            if (comp.ecommerce_conf.search_engine=="ETX_SEARCH") {
                module="ecommerce-search-v2.html";
                item_template="default-search.html";
            }
            if (comp.ecommerce_conf.search_engine=="ETX_RECOMMENDER") {
                module="ecommerce-recom-search.html";
                item_template="recom-item.html";
            }

        } else if (subtype=='CART_PLUGIN') {
            // v2 too ??
            module="ecommerce-cart.html";
        }

        var item_html=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-ecommerce/product-item/"+item_template, "utf8");

        var code=_.template(item_html);
        var compiled=code({
            compObj: comp,
            $INV: invtFields,
            $PCF: pcFields,
            $ECF: ecFields,
            $PME: pmeFields,
            $BLF: blFields,
            $MCF: mcFields,
            compobj: comp,
            item: {},
            TR: Locale.TR,
            moneyFmt: wbUtils.moneyFormater(),
        });

        comp.item_html = compiled;

        // ADD SHOPPING CART MODALS
        var compiled22='';
        if (!support_cache[comp.fields[pcFields.page_id]]) {
            setTimeout(function () {
                support_cache[comp.fields[pcFields.page_id]]=false;
            }, 5000);

            support_cache[comp.fields[pcFields.page_id]]=true;
            var shopping_cart_modals_html=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-ecommerce/shopping-cart-modals.html", "utf8");
            var code22=_.template(shopping_cart_modals_html);
           
            comp.category=JSON.parse(comp.fields[pcFields.category], '{"list": []}');

            compiled22=code22({
                compObj: comp,
                $INV: invtFields,
                $PCF: pcFields,
                $ECF: ecFields,
                $PME: pmeFields,
                $BLF: blFields,
                $MCF: mcFields,
                compobj: comp,
                TR: Locale.TR,
                moneyFmt: wbUtils.moneyFormater(),
            });
        }

        comp.shopping_cart_html = compiled22;
        // console.info("[APP_ECOMMERCE] subtype is %s ", subtype);
        // console.info("[APP_ECOMMERCE]::%s load module module %s", subtype, module);
        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-ecommerce/"+module, "utf8");

    } else if (comp_type=="APP_ECOMMERCE_ITEM") {
        var shopping_cart_modals_html=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-ecommerce/shopping-cart-modals.html", "utf8");
        var code22=_.template(shopping_cart_modals_html);
        var compiled22=code22({
            compObj: comp,
            $INV: invtFields,
            $PCF: pcFields,
            $ECF: ecFields,
            $PME: pmeFields,
            $BLF: blFields,
            $MCF: mcFields,
            compobj: comp,
            TR: Locale.TR,
            moneyFmt: wbUtils.moneyFormater(),
        });

        comp.shopping_cart_html = compiled22;
        comp.ecommerce_item_conf = JSON.parse(comp.fields[pcFields.conf_obj]||'{}');

        var module="ecommerce-item.html";
        var template=comp.ecommerce_item_conf.template;

        if (template=='ITEM_V2') {
            module="ecommerce-item-v2.html";

        } else if (template=='ITEM_V4') {
            module="ecommerce-item-v4.html";

        } else if (template=='ITEM_V3') {
            module="ecommerce-item-v3.html";
        }

        tpl=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-ecommerce/"+module, "utf8");

        if (!template || template=='DEFAULT') {
            var __mlt_recommender_html='';
            if (comp.ecommerce_item_conf.enable_recommender) {
                __mlt_recommender_html=fs.readFileSync(appRoot + "/webapp/templates/wb-comp/wb-ecommerce/more-like-this.html", "utf8");
            }

            tpl = tpl.replace("__mlt_recommender__", __mlt_recommender_html);
        }
    }

    return new Promise(function (resolve, reject) {
        function  _compileComp(PromResult) {
            var compiled='';
            if (tpl) {
                try {
                    var root_url=modsConf.c.WsHost()+"/webapp/"+webapp.id+"/view";
                    if (webapp.fields[wapFields.domain]) {
                        root_url=webapp.fields[wapFields.domain];
                    }

                    var code=_.template(tpl);
                    compiled=code({
                        $PCF: pcFields,
                        $ECF: ecFields,
                        $ENF: entFields,
                        $EXT: EXT,
                        $HSF: rsHostFields,
                        $WAF: waFields,
                        $WAPF: wapFields,
                        $SVF: svFields,
                        $BXF: bxFields,
                        $INV: invtFields,
                        $OTF: otFields,
                        $PME: pmeFields,
                        $BLF: blFields,
                        $MCF: mcFields,
                        $V4F: vt4Fields,
                        $BLF: blFields,
                        $RVF: rvFields,
                        moment: moment,
                        root_url: root_url,
                        orgObj: orgObj,
                        ETX_MARKET_ID: modsConf.c.ETX_MARKET(),
                        moneyFmt: wbUtils.moneyFormater(),
                        WS_HOST: modsConf.c.WsHost(),
                        STATIC_PATH: staticPath,
                        promRes: PromResult,
                        compobj: comp,
                        webpage: page,
                        webapp: webapp,
                        TR: Locale.TR,
                    });

                    resolve({
                        comp: comp,
                        compiled_html: compiled
                    });

                } catch (e) {
                    console.error("[ERROR] Could not compile component [%s:%s]", 
                        comp.fields[pcFields.name], 
                        comp.fields[pcFields.type]
                    );
                    console.error("[ERROR] More details ", e);
                    // console.error("[ERROR] Template %s", tpl);

                    reject({
                        // comp:comp, 
                        errors: [e.message]
                    });
                }
            } else {
                reject({comp:comp, errors: ["comp_type '"+comp_type+"' is not valid!"]});
            }
        }

        compPromise.then(function (Result) {
            _compileComp(Result);
        }, function (Err) {
            console.error("[ERROR] compPromise[%s] did not resolve!", comp_type);
            console.error("[ERROR-DETAILS] %o.", Err);
            _compileComp(null);
        });
    });
}


exports.generatePage=generatePage;
