const fetch=require('make-fetch-happen');
import _ from 'underscore';
import {etxTables,APP_MODE,conf} from '../api/modules-conf';
import * as compiler from "@marko/compiler";

const Inventory=conf.InventoryTable();
const invtFields=Inventory.fields();
const invtCols=Object.keys(invtFields).map(function (C) {
    return invtFields[C];
});

export function RenderPage() {
    return new Promise(function (resolve, reject) {
        FetchInventory()
        .then(function (invent) {
            var tpl=`<etx-marketplace items=input.inventory>
                </etx-marketplace>
            `;

            const syncResult = compiler.compileSync(
                tpl, 
                "./src/index.marko", {
                modules: "cjs",
                output: 'dom',
                input: {
                    items: invent
                }
            });

            var compiled = `
              <script type="text/javascript">
                ${syncResult.code}
              </script>
            `;

            resolve(compiled);
        }, reject);
    });
}

function reqFailed (res) {
    return function (error) {
        res.status(500)
        .send({
            "errors": error
        });
    }
}

function FetchInventory() {
    return new Promise(function (resolve, reject) {
        // var url='https://publica.enterstarts.com/ecommerce-cms/products/filter';
        var url='http://127.0.0.1:9000/ecommerce-cms/products/filter';

        var offset=_.random(0, 20);
        var limit = 20+offset;

        var reqData = {
            "orgId":"e22c394b31c74063",
            "appId":"342411964b674b1c",
            "offset":offset,
            "limit": limit,
            "category_list":[],
            "cartReq":{
                "idKey":"LT_BuOcD9H3qiq3BCJG91",
                "auth":"0IfC567B2#g'N@JLK;_4P766c8!*I~G^5H:E"
            }
        }

        var req={
            method: 'post',
            headers: {
                'Content-type': 'application/json',
            },
            body: JSON.stringify(reqData)
        };

        fetch(url, req)
        .then(function (Resp){
            Resp.json()
            .then(function (json){
                resolve(json);
            }, reject);
        }, reject);
    })
}

// exports = {
//   RenderPage: RenderPage  
// };