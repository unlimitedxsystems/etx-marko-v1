const fetch=require('make-fetch-happen');
import _ from 'underscore';
import {etxTables,APP_MODE,conf} from '../api/modules-conf';
import * as compiler from "@marko/compiler";
import * as appV1 from './app-v1';
import * as appV2 from './app-v2';
const Inventory=conf.InventoryTable();
const invtFields=Inventory.fields();
const invtCols=Object.keys(invtFields).map(function (C) {
    return invtFields[C];
});

export function RenderPage(BotAuth, AuthToken, context, req, res) {
    var text='arigato';
    return new Promise(function (resolve, reject) {
        console.info("[INFO] RenderPage():: %o ", context);
        var page=context.page;
        var hasHtml=page.indexOf(".html")!=-1;
        if (hasHtml) {
            context.page= page.slice(0, page.indexOf(".html"))
        }

        appV1.RenderPage(BotAuth, AuthToken, context)
        .then(function (compiled) {
            resolve(compiled);
        }, reject);
    });
}

function reqFailed (res) {
    return function (error) {
        res.status(500)
        .send({
            "errors": error
        });
    }
}

// exports = {
//   RenderPage: RenderPage  
// };