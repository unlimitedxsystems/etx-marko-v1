const modsConf=require('../api/modules-conf');
const APIService=require("../api/service").apiService;
const wbSection=require("./wb-section-v2");
const moment=require('moment');
const _=require('underscore');

var WebApp=modsConf.c.WebAppTable();
var waFields=WebApp.fields();

var WebAppPage=modsConf.c.WebAppPage();
var wapFields=WebAppPage.fields();
var wapCols=Object.keys(wapFields).map(function (C) {
    return wapFields[C];
});

var PageSection=modsConf.c.WebPageSections();
var psFields=PageSection.fields();
var psCols=Object.keys(psFields).map(function (C) {
    return psFields[C];
});

var PageComp=modsConf.c.WebPageComp();
var pcFields=PageComp.fields();
var pcCols=Object.keys(pcFields).map(function (C) {
    return pcFields[C];
});

function normalizePageName(string) {
    var name_= string.toLowerCase().trim();
    var first=name_[0].replace(/[^\w^\d]/g, '');
    var pageName=first+(name_.substr(1).replace(/[^\w^\d]/g, '-'));
    return pageName;
}

function getPageByName(Auth, appId, pageName_) {
    return new Promise(function (resolve, reject) {
        var start=modsConf.c.START_DATE();
        var end=modsConf.c.END_DATE();

        var cList = [
        ].concat(wapCols);

        var start=modsConf.c.START_DATE();
        var end=modsConf.c.END_DATE();

        var pageName=normalizePageName(pageName_);

        var searchJob={
            project: cList,
            from: [
                WebAppPage.id
            ],
            filter: [
                " "+wapFields.webapp_id+"='"+appId+"' ",
                " and "+wapFields.name+"='"+pageName+"'"
            ],
            timeframe: [start, end]
        };

        var limit=50;
        var offset=0;

        var api=new APIService("object", Auth);
        api.post("fysearch?limit="+limit+"&offset="+offset, searchJob)
        .then(function (Resp) {
            resolve(Resp);
        }, reject);
    });
}

function RenderPage (BotToken, AuthToken, context) {
    return new Promise(function (resolve, reject) {
        var api = new APIService("object", BotToken);
        var orgApi = new APIService("org", BotToken);
        var appId=context.appId;
        var page=context.page;

        var q=[
            api.get(appId),
            getPageByName(BotToken, appId, page)
        ];

        Promise.all(q)
        .then(function (Ary) {
            var appObj=Ary[0];
            var searchPageRes=Ary[1];
            if (searchPageRes.totalCount==0) {
                reject({
                    error: ["404", "PAGE_NOT_FOUND"]
                });
                return;
            }

            var pageObj=searchPageRes.list[0];
            var filterObj={
                pages: {
                    set: [pageObj.id]
                }
            }

            var q2=[
                fetchSections(BotToken, appId, filterObj),
                fetchComponents(BotToken, appId, filterObj),
                orgApi.get(appObj.org),
            ];

            Promise.all(q2)
            .then(function (Resp) {
                var sections=Resp[0];
                var comps=Resp[1];
                var orgObj=Resp[2];
                // var Locale=Resp[3];
                var Locale={
                    LANG: context.lang,
                    TR: function (key) {
                        return key;
                    }
                }
                var page_sections={};
                var sections_cache={};
                var scount=0;

                page_sections[pageObj.id]={
                    secs: [],
                    comps: []
                };

                sections.list.forEach(function (S){
                    var page_id=S.fields[psFields.page_id];

                    if (!page_sections[page_id]) {
                        page_sections[page_id]={
                            secs: [],
                            comps: []
                        };
                    }

                    if (!sections_cache[S.id]) {
                        sections_cache[S.id]=[];
                    }

                    page_sections[page_id].secs.push(S);
                });

                comps.list.forEach(function (C){
                    var page_id=C.fields[pcFields.page_id];
                    var comp_type=C.fields[pcFields.type];
                    var confObj_=C.fields[pcFields.conf_obj];
                    var confObj=JSON.parse(confObj_||'{}');
                    var comp_pages_list=confObj.pages||[];
                    var found=false;

                    if (!page_sections[page_id]) {
                        page_sections[page_id]={
                            secs: [],
                            comps: []
                        };
                    }

                    var section_id=C.fields[pcFields.section_id];            
                    if (sections_cache[section_id] || section_id=='GLOBAL_SECTION') {
                        page_sections[page_id].comps.push(C);

                        if (sections_cache[section_id]) {
                            sections_cache[section_id].push(C);
                        }
                    }

                    comp_pages_list.forEach(function (P) {
                        if (!page_sections[P]) {
                            page_sections[P]={
                                secs: [],
                                comps: []
                            };
                        }
                        page_sections[P].comps.push(C);
                    });

                    Object.keys(page_sections).forEach(function (K){
                        var psection=page_sections[K];
                        // order section by order_inc
                        var ordered=_.sortBy(psection.secs, function (S){
                            var order=parseInt(S.fields[psFields.order]||0);
                            if (isNaN(order)) {
                                order=scount++;
                            }
                            return order;
                        });
                        page_sections[K].secs=ordered;
                    });
                });

                wbSection.generatePage(
                    BotToken, orgObj,
                    appObj, pageObj, 
                    [pageObj],
                    page_sections[pageObj.id].secs,
                    page_sections[pageObj.id].comps,
                    false,
                    Locale,
                    context.env
                ).then(function (generated_html) {
                    resolve(generated_html);
                }, reject);

            }, reject);

        }, reject);
    });
}

function fetchSections(Auth, appId, filterObj) {
    return new Promise(function (resolve, reject) {
        var cList = [
        ].concat(psCols);

        var pages_filter=false;
        var filterConds = [
            psFields.webapp_id+"='"+appId+"' ",
        ];

        if (filterObj.conds && filterObj.conds.length>0) {
            filterConds=filterConds.concat(filterObj.conds);
        }

        if (filterObj&& filterObj.pages) {
            if (filterObj.pages.set=='ALL') {

            } else if (_.isArray(filterObj.pages.set)) {
                pages_filter = filterObj.pages.set.map(function (pageId) {
                    return "'"+pageId+"'";
                }).join(",");
            }
        }

        // console.info("[INFO] fetchSections(): pages_filter=%s", pages_filter);

        if (pages_filter) {
            if (pages_filter.length==1) {
                filterConds.push(
                    " and ZO."+psFields.page_id+" = '"+pages_filter[0]+"' "
                );

            } else {
                filterConds.push(
                    " and ZO."+psFields.page_id+" IN ("+pages_filter+") "
                );
            }
        }

        filterConds.push(" and deleted=false");

        var start=modsConf.c.START_DATE();
        var end=modsConf.c.END_DATE();

        var searchJob={
            project: cList,
            from: [
                PageSection.id
            ],
            filter: filterConds,
            timeframe: [start, end]
        };

        var limit=300;
        var offset=filterObj.offset||0;

        // console.info("[INFO] fetchSections(): ", searchJob);

        var api=new APIService("object", Auth);
        api.post("fysearch?limit="+limit+"&offset="+offset, searchJob)
        .then(function (Resp) {
            resolve(Resp);
        }, reject);
    });
}

function fetchComponents(Auth, appId, filterObj) {
    filterObj = filterObj||{};

    return new Promise(function (resolve, reject) {
        var cList = [
        ].concat(pcCols);

        var pages_filter=false;
        var filterConds = [
            pcFields.webapp_id+"='"+appId+"' ",
        ];

        if (filterObj.conds && filterObj.conds.length>0) {
            filterConds=filterConds.concat(filterObj.conds);
        }

        if (filterObj&& filterObj.pages) {
            if (filterObj.pages.set=='ALL') {
                // more complicated

            } else if (_.isArray(filterObj.pages.set)) {
                pages_filter = filterObj.pages.set.map(function (pageId) {
                    return "'"+pageId+"'";
                }).join(",");

                var pages_like = filterObj.pages.set.map(function (pageId) {
                    // return "'"+pageId+"'";
                    return pcFields.conf_obj + " LIKE '%"+pageId+"%' ";
                }).join(" or ");
    
                filterConds.push(
                    " and ("+
                        pcFields.page_id+" IN ("+pages_filter+") or "+
                        "(" + pages_like + ")"+
                    ")"
                );
            }
        }

        filterConds.push(" and deleted=false");

        var start=modsConf.c.START_DATE();
        var end=modsConf.c.END_DATE();

        var searchJob={
            project: cList,
            from: [
                PageComp.id
            ],
            filter: filterConds,
            timeframe: [start, end]
        };

        var limit=300;
        var offset=filterObj.offset||0;

        // console.info("[INFO] fetchComponents(): ", searchJob);

        var api=new APIService("object", Auth);
        api.post("fysearch?limit="+limit+"&offset="+offset, searchJob)
        .then(function (Resp) {
            resolve(Resp);
        }, reject);
    });
}

exports.RenderPage = RenderPage;