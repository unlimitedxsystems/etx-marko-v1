const _=require('underscore');
const moment=require('moment');
const modsConf=require('../api/modules-conf');
const APIService=require('../api/service').apiService;

const fs = require('fs');
const exec=require('child_process').exec;
const MODULE="webapp-service/utils";

var PageComp=modsConf.c.WebPageComp();
var pcFields=PageComp.fields();

function ProcessCompOptmizerConf (compObj, runConf) {
    console.info("[INFO] %s [%s] ProcessCompOptmizerConf(%s[%s]) (enable_compr=%s, compr_settings=%s) ",
        MODULE,
        compObj.fields[pcFields.type],
        compObj.fields[pcFields.data_policy],
        moment().format("YYYY-MM-DDTHH:mm:ss"),
        compObj.fields[pcFields.enable_compr],
        compObj.fields[pcFields.compr_settings],
    );

    if (!compObj) {
        return false;
    }
    if (!runConf) {
        return false;
    }
    if (!runConf.image) {
        return false;
    }

    if (!compObj.fields[pcFields.compr_settings] ||
        !compObj.fields[pcFields.enable_compr]) {
        return;
    }

    var defaultCDN=modsConf.c.PUBLICA_IMAGE_CDN();

    try {
        var compressor=JSON.parse(compObj.fields[pcFields.compr_settings]||'{}');
        // console.info("compressor:: ", compressor);

        var width=compressor.width||350
        var height=compressor.height||350
        var resize_mode=compressor.resize_mode||"cover";
        var bucket=compressor.s3_bucket||"etx-storage"; 
        var compCloudfrontDist=compObj.fields[pcFields.cloudfront];

        var img=runConf.image||'';
        var has_cloudfront=img.indexOf("cloudfront.net");
        var img_source=''; // ON-DEMAND | COMPONENT

        if (img.startsWith('https://etx-storage.s3')) {
            // console.info("img: ", img);
            var slc=img.slice(10); // TODO: fix
            var imagekx=slc.slice(slc.indexOf("/")+1);
            var imageKey = decodeURIComponent(imagekx);
            var Img_PAN = {
                bucket: bucket,
                key: imageKey,
                edits: {
                    resize: {
                        width: width,
                        height: height,
                        fit: resize_mode
                    }
                }
            };

            if (compressor.fill_color) {
                Img_PAN.edits.resize.background = hexToRgbA(compressor.fill_color, 1);
            }

            if (compressor.background_color) {
                Img_PAN.edits.flatten={ background: hexToRgbA(compressor.background_color, undefined) };
            }

            // https://d22xdvpsxig1t2.cloudfront.net/index.html
            // console.info("Img_PAN: ", Img_PAN);
            // XXX: https://stackoverflow.com/questions/23097928/node-js-throws-btoa-is-not-defined-error
            // BROWSER-ONLY: btoa(JSON.stringify(Img_PAN));
            // console.log(Buffer.from('Hello World!').toString('base64'));
            // console.log(Buffer.from(b64Encoded, 'base64').toString());
            var b64=Buffer.from(JSON.stringify(Img_PAN)).toString('base64');
            var gen=defaultCDN+"/"+b64;            

            return {
                cloudfront_dist: defaultCDN,
                b64: b64,
                gen: gen,
                sz: width+'X'+height,
            };

        } else if (has_cloudfront!=-1) {
            console.info("already has cloudfront: ", img);
            var detectedCFN=img.indexOf(0, img.slice(has_cloudfront).indexOf("/"));
            return {
                Img_PAN: Img_PAN,
                cloudfront_dist: detectedCFN, 
                // cloudfront_dist: defaultCDN, 
                b64: img.slice(img.slice(has_cloudfront).indexOf("/")),
                gen: img,
                sz: width+'X'+height,
            };
        } else {
            console.info("[ERROR] %s [%s] Unexpected image ::ProcessCompOptmizerConf(%s[%s]) ",
                MODULE,
                compObj.fields[pcFields.type],
                compObj.fields[pcFields.data_policy],
                moment().format("YYYY-MM-DDTHH:mm:ss"),
                img
            );

            return false;
        }

    } catch (e) {
        // console.info("[ERROR] %s [%s] Failed to execute procedure::ProcessCompOptmizerConf(%s[%s]) ",
        //     MODULE,
        //     compObj.fields[pcFields.type],
        //     compObj.fields[pcFields.data_policy],
        //     moment().format("YYYY-MM-DDTHH:mm:ss"),
        //     e
        // );
        return false;
        // EmitEventLog (transient)
    }
}

function hexToRgbA(hex, _alpha) {
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return { r: ((c>>16)&255), g: ((c>>8)&255), b: (c&255), alpha: Number(_alpha)};
    }
    throw new Error('Bad Hex');
}

function ProcessCompressorConf (compressor, runConf) {
    // { 
    //   img_source: 'COMPONENT',
    //   resize_mode: 'cover',
    //   width: 1242,
    //   height: 443,
    //   fill_color: 'fill-33',
    //   h_grayscale: true 
    // }
    console.info("[INFO] %s [%s] ProcessCompressorConf() ",
        MODULE,
        moment().format("YYYY-MM-DDTHH:mm:ss"),
        compressor
    );

    var defaultCDN=modsConf.c.PUBLICA_IMAGE_CDN();

    try {
        var width=compressor.width||350
        var height=compressor.height||350
        var resize_mode=compressor.resize_mode||"cover";
        var bucket=compressor.s3_bucket||"etx-storage"; 
        // var bucket=compressor.s3_bucket||"etx-storage"; 
        var img=runConf.image||'';
        var has_cloudfront=img.indexOf("cloudfront.net");
        var img_source=''; // ON-DEMAND | COMPONENT

        if (img.startsWith('https://etx-storage.s3')) {
            // console.info("img: ", img);
            var slc=img.slice(10); // TODO: fix
            var imagekx=slc.slice(slc.indexOf("/")+1);
            var imageKey = decodeURIComponent(imagekx);
            var Img_PAN = {
                bucket: bucket,
                key: imageKey,
                edits: {
                    resize: {
                        width: width,
                        height: height,
                        fit: resize_mode
                    }
                }
            };

            if (compressor.fill_color) {
                Img_PAN.edits.resize.background = hexToRgbA(compressor.fill_color, 1);
            } else {
                // transparent
                Img_PAN.edits.resize.background = hexToRgbA("#fffff", undefined);
            }

            // if (compressor.background_color) {
            //     Img_PAN.edits.flatten={ background: hexToRgbA(compressor.background_color, undefined) };
            // }

            var b64=Buffer.from(JSON.stringify(Img_PAN)).toString('base64');
            var gen=defaultCDN+"/"+b64;

            return {
                cloudfront_dist: defaultCDN,
                b64: b64,
                gen: gen,
                sz: width+'X'+height,
            };
        } else if (has_cloudfront!=-1) {
            var detectedCFN=img.indexOf(0, img.slice(has_cloudfront).indexOf("/"));
            return {
                cloudfront_dist: detectedCFN, 
                // cloudfront_dist: defaultCDN, 
                b64: img.slice(img.slice(has_cloudfront).indexOf("/")),
                gen: img,
                sz: width+'X'+height,
            };
        } else {
            console.info("[ERROR] %s [%s] Unexpected image ::ProcessCompressorConf() ",
                MODULE,
                moment().format("YYYY-MM-DDTHH:mm:ss"),
                img
            );

            return false;
        }

    } catch (e) {
        // console.info("[ERROR] %s [%s] Failed to execute procedure::ProcessCompressorConf:: ",
        //     MODULE,
        //     moment().format("YYYY-MM-DDTHH:mm:ss"),
        //     e
        // );
        return false;
        // EmitEventLog (transient)
    }
}

function moneyFormater(currencyCode) {
    var COIN=currencyCode||'CVE';
    var NF = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: COIN,
    });

    return function formatter (value) {
        var fmt=NF.format(value);
        return fmt.replace(new RegExp(COIN),'');
    }
}

function normalizePageName(string) {
    var name_= string.toLowerCase().trim();
    var first=name_[0].replace(/[^\w^\d]/g, '');
    var pageName=first+(name_.substr(1).replace(/[^\w^\d]/g, '-'));
    return pageName;
}


// exports.promFinally=promFinally;
exports.ProcessCompOptmizerConf=ProcessCompOptmizerConf;
exports.ProcessCompressorConf=ProcessCompressorConf;
exports.normalizePageName=normalizePageName;
exports.moneyFormater=moneyFormater;
