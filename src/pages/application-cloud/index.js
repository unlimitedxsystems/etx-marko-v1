import template from "./template.marko";
import fetch from 'node-fetch'
const _=require('underscore');
const moment=require('moment');
const modsConf = require('../../api/modules-conf');
const APIService = require('../../api/service').apiService;

var WebApp=modsConf.c.WebAppTable();
var waFields=WebApp.fields();
var waCols=Object.keys(waFields).map(function (C) {
    return waFields[C];
});
const $WAF=waFields;

var WebAppPage=modsConf.c.WebAppPage();
var wapFields=WebAppPage.fields();
var wapCols=Object.keys(wapFields).map(function (C) {
    return wapFields[C];
});

function fromToken(Auth) {
    var api=new APIService("user", Auth);
    return api.get("me");
}


export default (req, res) => {
    var token=req.headers['authorization'];
    var appId=req.params.appId;

    var api=new APIService("object", token);
    var q= [
        fromToken(token),
        FetchApps(token)
    ];

    Promise.all(q)
    .then(function (Ary) {
        var Resp=Ary[0];
        var appList=Ary[1];
        var authUser={
            id: Resp.id,
            email: Resp.email,
            org: Resp.org,
            name: Resp.name,
            type: Resp.type,
            state: Resp.state
        };
        var params={
            authUser: authUser, 
            APIService: APIService, 
            Token: token,
            appList: appList
        };
        res.marko(template, params);
    
    }, function (Err) {
        var status=Err.data&&Err.data.status ? Err.data.status : 400;
        res.status(status)
            .send(Err);
    });
};

function FetchApps (AuthToken) {
    return new Promise(function (resolve, reject) {
        var start=modsConf.c.START_DATE();
        var end=modsConf.c.END_DATE();

        var filterConds = [
        ];

        var searchJob={
            project: waCols,
            from: [
                WebApp.id,
            ],
            filter: filterConds,
            timeframe: [start, end]
        };

        var limit=50;
        var offset=0;

        var api=new APIService("object", AuthToken);
        api.post("fysearch?limit="+limit+"&offset="+offset, searchJob)
        .then(function (Resp) {
            resolve(Resp);
        }, function (Err) {
            console.error("[fetchProductsIn-ERROR]: ", Err);
            reject(Err);
        });

    });
}
