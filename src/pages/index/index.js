import template from "./template.marko";

export default (req, res) => {
    try {
          res.marko(template, {});
    } catch (e) {
        console.error("ERROR on rendering: ", e);
        res.status(400)
            .send({errors:['RENDERING_ENGINE']})
    }
};
