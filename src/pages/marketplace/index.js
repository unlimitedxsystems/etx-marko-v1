// import '@ebay/skin/breadcrumbs';
import template from "./template.marko";
const fetch=require('make-fetch-happen');
import _ from 'underscore';
import {etxTables,APP_MODE,conf} from '../../api/modules-conf';

const Inventory=conf.InventoryTable();
const invtFields=Inventory.fields();
const invtCols=Object.keys(invtFields).map(function (C) {
    return invtFields[C];
});

export default (req, res) => {
    return new Promise(function (resolve, reject) {
        var url=conf.WsHost()+'/ecommerce-cms/products/filter';
        var offset=_.random(0, 20);
        var limit = 20+offset;

        var reqData = {
            "orgId":"e22c394b31c74063",
            "appId":"342411964b674b1c",
            "offset":offset,
            "limit": limit,
            "category_list":[],
            "cartReq":{
                "idKey":"LT_BuOcD9H3qiq3BCJG91",
                "auth":"0IfC567B2#g'N@JLK;_4P766c8!*I~G^5H:E"
            }
        }

        var req={
            method: 'post',
            headers: {
                'Content-type': 'application/json',
            },
            body: JSON.stringify(reqData)
        };

        fetch(url, req)
        .then(function (Resp){
            Resp.json()
            .then(function (json){
                 res.marko(template, {
                    name: "Ayrton Gomes",
                    inventory: json
                 });
            }, reqFailed(res));

        }, reqFailed(res));
    });
};

function reqFailed (res) {
    return function (error) {
        res.status(500)
        .send({
            "errors": error
        });
    }
}
