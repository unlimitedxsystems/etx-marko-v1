import template from "./template.marko";
import fetch from 'node-fetch'
const _=require('underscore');
const moment=require('moment');
const modsConf = require('../../api/modules-conf');
const APIService = require('../../api/service').apiService;

function fromToken(Auth) {
    var api=new APIService("user", Auth);
    return api.get("me");
}

// TODO: implement api-base authentication
// TO REMOVE passing allong the authorization
export default (req, res) => {
    return new Promise(function (resolve, reject) {
        var token=req.headers['authorization'];
        var appId=req.params.appId;

        var api=new APIService("object", token);
        var q =[
            api.get(appId),
            fromToken(token)
        ]

        Promise.all(q)
        .then(function (Ary) {
            var appObj=Ary[0];
            var Resp=Ary[1];
            if (appObj.org!=Resp.org) {
                reject({
                    errors: ["APP_NOT_FOUND"]
                });
                return;
            }

            FetchPages(token, appObj.id)
            .then(function (pageList) {
                console.info("[INFO] User is Authenticated [%s#%s#s]", Resp.name, Resp.email, Resp.org);
                var authUser={
                    id: Resp.id,
                    email: Resp.email,
                    org: Resp.org,
                    name: Resp.name,
                    type: Resp.type,
                    state: Resp.state
                };
                var params={
                    authUser: authUser, 
                    APIService: APIService, 
                    Token: token,
                    appObj: appObj,
                    pageList: pageList
                };
                res.marko(template, params);
                resolve();

            }, function (Err) {
                console.error("ERR: ", Err);
                var status=Err.data&&Err.data.status ? Err.data.status : 400;
                res.status(status)
                    .send(Err);
                reject(Err);
            });

        }, function (Err) {

            console.error("ERR: ", Err);
            var status=Err.data&&Err.data.status ? Err.data.status : 400;
            res.status(status)
                .send(Err);
            reject(Err);
        });
    });
};

function FetchPages (AuthToken, appId) {
    return new Promise(function (resolve, reject) {
        var req={
          method: 'post',
          headers: {
              'Content-type': 'application/json',
              'authorization': AuthToken
          }
        };

        var url=modsConf.c.WsHost()+"/webapp/"+appId+"/pages/filter";
        var request={
            limit: 150
        };
        req.body = JSON.stringify(request);
        console.info("[INFO] New Request [%s]", url);

        fetch(url, req)
        .then(function (resp) {
            modsConf.ProcessResponse(resp)
            .then(function (data){
                console.info("Got %s Pages for %s: ", data.totalCount, appId);
                resolve(data);
            }, function (error){
                console.error("[ERROR] ProcessResponse: %o", error);
                reject(error);
            });

        }, function (error){
            console.error("[ERROR] fetch: %o", error);
            reject(error);
        });
    });
}
