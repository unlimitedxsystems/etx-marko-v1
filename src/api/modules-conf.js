// WARNING
// @verify static/js/app.js also
// var APP_MODE="testnet";
// var APP_MODE="stage";
var APP_MODE="online";
var func;

var onlineModules=require("./modules-conf-online");
var stateModules=require("./modules-conf-stage");
var conf;

if (APP_MODE=="online") {
    conf=onlineModules.conf;

} else if (APP_MODE=="stage") {
    conf=stateModules.conf;

} else {
    console.warn(
        "[WARNING] Unexpected APP_MODE: '%s'\nAssuming online", 
        APP_MODE
    );

    APP_MODE="online";
    conf=onlineModules.conf;
}

exports.tables = conf;
exports.conf = conf;
exports.c = conf;

exports.APP_MODE = function () {
    return APP_MODE;
}

exports.IsOnline = function () {
    return APP_MODE=="online";
}

exports.APP_VERSION = function () {
    return 'v.1.9.8-rosebeth 2020-04-06 08:20AM';
}

exports.ProcessResponse = function (Resp) {
    return new Promise(function (resolve, reject) {
        if (Resp.ok) {
            Resp.text()
            .then(function (textResponse) {
                try {
                    var json=JSON.parse(textResponse);
                    resolve(json);
                } catch (e) {
                    resolve(textResponse);
                }
            }, reject);

        } else {
            Resp.text()
            .then(function (textResponse) {
                var response=textResponse;
                try {
                    response=JSON.parse(textResponse);
                    reject({
                        statusText: Resp.statusText,
                        status: Resp.status,  
                        code: Resp.status,
                        data: response
                    });
                    return;
                } catch (e) {
                }

                reject({
                    statusText: Resp.statusText,
                    status: Resp.status,                   
                    code: Resp.status,    
                    data: {errors: [textResponse]}
                });

            }, function (err) {
                reject({
                    statusText: Resp.statusText,
                    status: Resp.status,
                    code: Resp.status,
                    data: {errors: [Resp.statusText]}
                });
            });

        }
    });
}
