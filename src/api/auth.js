#!/usr/bin/env node
const fetch=require('node-fetch');
const modsConf = require('./modules-conf');
var host = modsConf.conf.apiHost();

// var FundsManagement=modsConf.c.FundsManagement();
var Accounts=modsConf.c.AccountsTable();
var acFields=Accounts.fields();

const TOKEN_FILE='.app-token';

function getToken(user) {
    var promise=new Promise(function (resolve, reject) {
        fs.readFile(TOKEN_FILE, 'utf-8' ,function (err, buf) {
            var content=buf;
            if (!content) {
                return loginAndSaveToken(user);
            } else {
                var auth=JSON.parse(buf.toString());
                me(auth.reqToken)
                .then(function(){
                    resolve(auth);
                }, function (err) {
                    console.log('[WARN] Bad token. renewing');
                    return loginAndSaveToken();
                }, reject);
            }
        });
    });

    return promise;
}

function loginAndSaveToken(user) {
    return new Promise(function (resolve, reject){    
        login(user)
        .then(function (auth) {
            fs.writeFile(TOKEN_FILE, JSON.stringify(auth), function(err, data){
                if (err) {
                    console.log('[ERROR] %o', err);
                    process.exit(1);
                }
                console.log("[INFO] saved token.");
                resolve(auth);
            }, reject);
        }, reject);
    })
}

function login(userData) {
    return new Promise(function (resolve, reject) {
        fetch(host+'/api/v1/user/signin', {
            method:'POST',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify(userData)
        }).then(function (Data){
            Data.json().then(function (Resp){
                if (Resp.token) {
                    resolve(Resp);
                } else {
                    console.error('[ERROR] could not login, req: %o', userData);
                    console.error('[ERROR] Details %o', Resp);
                    reject(Resp);
                }
            }, reject);
        }, reject);
    })
}

function me(token) {
    return new Promise(function (resolve, reject) {
        fetch(host+'/api/v1/user/me', {
            method:'GET',
            headers: {'Content-type': 'application/json',
                     'Authorization': token},
        }).then(function (Data){
            Data.json()
            .then(function (Resp) {
                resolve();
            }, reject);
        }, reject);
    });
}

function fetchObj_(id, token) {
    return new Promise(function (resolve, reject) {
        fetch(host+'/api/v1/object/'+id, {
            method:'GET',
            headers: {'Content-type': 'application/json',
                     'Authorization': token},
        }).then(function (Data){
            Data.json()
            .then(resolve, reject);
        }, reject);
    });
}

function fetchUser_(id, token) {
    return new Promise(function (resolve, reject) {
        fetch(host+'/api/v1/user/'+id, {
            method:'GET',
            headers: {'Content-type': 'application/json',
                     'Authorization': token},
        }).then(function (Data){
            Data.json()
            .then(resolve, reject);
        }, reject);
    });
}

function saveAccount (obj, token) {
    return new Promise(function (resolve, reject) {
        var url=host+'/api/v1/object/new';
        var method='POST';
        if (obj.id) {
            method='PUT';
            url=host+'/api/v1/object/'+obj.id;
        }

        fetch(url, {
            method:method,
            headers: {'Content-type': 'application/json',
                     'Authorization': token},
            body: JSON.stringify(obj)
        }).then(function (Data){
            Data.json()
            .then(function (Obj) {
                if (!Obj.id) {
                    reject(Obj);
                } else {
                    resolve(Obj);
                }
            }, reject);
        }, reject);
    });
}

function getAccount(userid, token) {
    return new Promise(function (resolve, reject) {
        fetchUser_(userid, token)
        .then(function (userObj) {
            fetchObj_(userObj.schemaRef, token)
            .then(function (Resp) {
                resolve({
                    user: userObj,
                    account: Resp
                });
            }, reject);
        }, reject);
    });
}

// exports.signin = login;
// exports.me = me;
exports.getToken=getToken;
exports.login=login;
exports.account=getAccount;
exports.saveAccount=saveAccount;