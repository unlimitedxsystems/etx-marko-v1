var host="127.0.0.1";
// var host="192.168.88.222";
// var host="192.168.33.168";
// var host="192.168.43.201";
// var host="192.168.1.3";
// var host="172.20.10.12";
// var host="192.168.1.101";

function getApi() {
    // return 'https://api.enterstarts.com/api/v1';
    // return 'http://172.31.26.191:5000/api/v1';
    // return 'http://192.168.32.201:5000';
    return 'http://'+host+':5000/api/v1';
}

function getApiHost() {
    // return 'https://api.enterstarts.com';
    // return 'http://172.31.26.191:5000';
    // return 'http://192.168.32.201:5000';
    return 'http://'+host+':5000';
}

function getAuthApi() {
    // return 'https://api.enterstarts.com/api/v1/user';
    // return 'http://172.31.26.191:5000/api/v1/user';
    return 'http://'+host+':5000/api/v1/user';
}

function getMetricsHost() {
    // return 'http://172.31.26.191:5001';
    return 'http://'+host+':5001';
}

/**
 * Tables
 */
function getMessagesTable () {
    return {
        id: '075931cce7ef416e',
        fields: function () {
            return {
                to: 'col1',
                from: 'col2',
                msg: 'col3',
                subject: 'col4',
                channel: 'col5',
                channel_id: 'col6',
                time: 'col7',
                user_status: 'col8',
                context: 'col9',
                category: 'col10',
                context_id: 'col11',
                user_name: "col12"
             }
        }
    }
}

function getAccountsTable() { 
    return {
        id: '8acb95a3eb0941bb', 
        fields: function() {
            return { 
                user_id: 'col1', 
                issue_biz_id: 'col2', 
                last_seen: 'col3', 
                referral: 'col4', 
                source: 'col5', 
                user_name: 'col6',
                state: 'col7',
                device_token: 'col14'
            } 
        }
    } 
}

function getServiceRequestTbl() {
	return {
		id:"276d91cf682f4cf5", // online
		// id: "0f30ec6ad94e49eb",
		fields: function() { 
			return {
				org_id: "col1",
				service_id: "col2",
				user_name: "col3",
				user_phone: "col4",
				user_email: "col5",
				user_msg: "col6",
                user_id: "col7",
                service_descr: "col8",
                user_address: 'col9',
                channel: 'col10',
                quantity: "col11",
                customer_id: "col12",
                app_id: "col13",
                page_id: "col14",
                comp_id: "col15",
                user_surname: "col16"
			}
		}		
	}
}

function getCustomerTable() {
	return {
		id:"2a5f8a3898ac4e81",
		fields: function() { 
			return {
				name: "col1",
				email: "col2",
				fbLink: "col3",
				twLink: "col4",
				referral: "col5",
				tags: "col6",
				date_of_birth: "col7",
				country: "col8",
				city: "col9",
				org: "col10",
				source: "col11",
				phone1: "col12",
				phone2: "col13",
				phone3: "col14",
				mobile1: "col15",
				mobile2: "col16",
				mobile3: "col17",
				user_id: "col18",
				address: "col19"
			}
		}		
	}
}

function getFilestore() {
    return {
        id:"881f8dfd57bc42f5",
        fields: function() { 
            return {
                filename: "col1",
                filetype: "col2",
                object_id: "col3",
                object_type: "col4",
                description: "col5",
                mime_type: "col6",
                filelabel: "col7",
                storetype: "col8",
                download_url: "col9",
                container: "col10",
                cover_url: "col11",
                cover_img_id: "col13",
                is_img: "col14"
            }
        }       
    }
}

function getEntityTable() {
    return {
        id:"a3d3ab9b3c90426a",
        fields: function() { 
            return {
                type: "col1",
                org_id: "col2",
                title: "col3",
                description: "col4",
                context: "col5",
                context_id: "col6",
                category: "col7",
                cover: "col8",
                pub_by: "col9",
                pub_by_label: "col10",
                view_link: "col11",
                view_link2: "col12",
                payment_ref: "col13",
                state: "col14",
                audience: "col15",
                app_id: "col16",
                object_id: "col17",
                page_id: "col18",
                comp_id: "col19"
            }
        }       
    }
}

var OBJ_STATE={
    ACOMP: {
        title: "Em Acompanhamento",
        id: 'ACOMP'
    },
    TREAT: {
        title: "Em tratamento",
        id: 'TREAT'
    },
    FAVOR: {
        title: "Favoravel",
        id: 'FAVOR'
    },
    NFAVOR: {
        title: "Não Favoravel",
        id: 'NFAVOR'
    },
    THIRDP: {
        title: "Competencia de Terceiros",
        id: 'THIRDP'
    },
    PARTEC: {
        title: "Parecer Tecnico",
        id: 'PARTEC'
    },
    PARSCI: {
        title: "Parecer Cientifico",
        id: 'PARSCI'
    },
    PARANX: {
        title: "Parecer Cientifico",
        id: 'PARSCI'
    },
    RESPOND: {
        title: "Respondido",
        id: 'RESPOND'
    },
    NRESPOND: {
        title: "Sem Resposta",
        id: 'NRESPOND'
    },
    ANALYSIS:{
        title: "Analise",
        id: "ANALYSIS"
    },
    DECISION:{
        title: "Decisão",
        id: "DECISION"
    },
    ACTION:{
        title: "Ação",
        id: "ACTION"
    },
    CONCLUDED:{
        title: "Concluida",
        id:'CONCLUDED'
    },
    NCONFORM: {
        title: "Não Conformidade",
        id: "NCONFORM"
    }
};

function getIReclamation(){
    return {
        id:"ad8fe30be3e6401c",
        fields: function() { 
            return {
                fact_date: "col1",
                name: "col2",
                direction: "col3",
                type: "col4",
                type_data: "col5",
                fact_details: "col6",
                action_date: "col7",
                action_details: "col8",
                action_resp: "col9",
                analysis_date: "col10",
                analysis_details: "col11",
                analysis_resp: "col12",
                decision_date: "col13",
                decision_details: "col14",
                decision_resp: "col15",
                state: "col16",
                resp_id: "col17",
                code: "col18",
                resp_prev: "col19"
            }
        }       
    }
}

function getComplaintTable() {
    return {
        id:"68deb97950d3461a",
        fields: function() { 
            return {
                name: "col1",
                address: "col2",
                local: "col3",
                phone: "col4",
                email: "col5",
                type: "col6",
                sector: "col7",
                sector_c1: "col8",
                op_name: "col9",
                op_address: "col10",
                op_local: "col11",
                op_phone: "col12",
                op_email: "col13",
                cmp_date: "col14",
                cmp_local: "col15",
                cmp_desc: "col16",
                cmp_deliver: "col17",
                state: "col18",
                parc_id: "col19",
                resp_id: "col20",
                code: "col21",
                resp_prev: "col23",
                sector_cat: "col24",
                sector_cat_descr: "col25",
                product_annex: "col26"
            }
        }       
    }
}

function getCompbookTable() {
    return {
        id:"1df9ba4c11f245c6",
        fields: function() { 
            return {
                name: "col1",
                address: "col2",
                nationality: "col3",
                phone: "col4",
                email: "col5",
                type: "col6",
                passport: "col8",
                DOC_ID: "col9",
                sup_name: "col10",
                sup_address: "col11",
                date: "col12",
                description: "col13",
                state: "col14",
                resp_id: "col15",
                code: "col16",
                resp_prev: "col17"
            }
        }       
    }
}

function getSugestionTable () {
    return {
        id:"9825b555fc2e4db2",
        fields: function() { 
            return {
                name: "col1",
                address: "col2",
                phone: "col3",
                email: "col4",
                department: "col5",
                type: "col6",
                date: "col7",
                description: "col8",
                state: "col9",
                parc_id: "col10",
                resp_id: "col11",
                code: "col12",
                resp_prev: "col13"
            }
        }       
    }
}

function getWebPage_Comp() {
    return {
        id:"9ff0cc974a9f4df3",
        fields: function() { 
            return {
                name: "col1",
                type: "col2",
                html_code: "col3",
                conf_obj: "col4",
                page_id: "col5",
                section_id: "col6",
                webapp_id: "col7",
                conf_obj_id: "col8",
                data_policy: "col9",
                title_df: "col10",
                image: "col11",
                navbar_logo: "col12",
                colsize: "col13",
                readmore_link: "col14",
                order: "col15",
                descr_df: "col16",
                phone_1: "col17",
                phone_2: "col18",
                phone_3: "col19",
                phone_4: "col20",
                email_1: "col21",
                email_2: "col22",
                fb_link: "col23",
                tw_link: "col24",
                insta_link: "col25",
                youtube_link: "col26",
                hq: "col27",
                op_hours: "col28",
                contact_logo: "col29",
                comp_descr: "col30",
                amount: "col31",
                progress: "col32",
                currency: "col33",
                img_w: "col34",
                img_h: "col35",
                user_id: "col36",
                org_id: "col37",
                data_policy_filter: "col38",
                data_policy_fetch: "col39",
                data_policy_schema: "col40",
                data_policy_objectid: "col41",
                checkout_page: "col42",
                view_page: "col43",
                image_id: "col44",
                checkout_page_id: "col45",
                view_page_id: "col46",
                comp_size: 'col47',
                tab_id: 'col48',
                category: 'col49',
                back_link: "col50",
                enable_compr: "col51",
                compr_settings: "col52",
                channel: "col53",
                cloudfront: "col54",
                payment_conf: "col55",
                comp_style: "col56"
            }
        }       
    }
}

function getWebAppPage_Sections() {
    return {
        id:"c1ace8dbe5aa468c",
        fields: function() { 
            return {
                name: "col1",
                colsize: "col2",
                background_color: "col3",
                background_cover: "col4",
                order: "col5",
                page_id: "col6",
                webapp_id: "col7",
                container_size: "col8",
                tab_id: "col9",
                hidden: "col10",
                url_vhtml_snippet: "col11",
                vhtml_snippet: "col12"
            }
        }       
    }
}

function getWebAppPage() {
    return {
        id:"23e1105bb2a34b2f",
        fields: function() { 
            return {
                name: "col1",
                title_df: "col2",
                descr_df: "col3",
                webapp_id: "col4",
                org: "col5",
                order: "col6",
                page_type: "col7",
                has_payment: "col8",
                paypal_client_id: "col9",
                bg_color: "col10",
                tracking_js: "col11",
                pg_keywords: "col12",
                pg_author: "col13",
                VURL: "col14",
                VIEWNAME: "col15",
                VCONTROLLER: "col16",
                VSTATE: "col17",
                VTEMPLATE: "col18",
                favicon: "col19",
                image: "col20",
                origin: "col21",
                head_code: "col22"
            }
        }       
    }
}

function getWebAppTplTable() {
    return {
        id:"ffb6357903c748b0",
        fields: function() { 
            return {
                title_df: "col1",
                descr_df: "col2",
                price: "col3",
                url: "col4",
                namespace: "col5",
                picture: "col6"
            }
        }       
    }
}

function getWebAppTbl() {
    return {
        id:"9b0f050733714287",
        fields: function() { 
            return {
                title_df: "col1",
                title_en: "col2",
                title_fr: "col3",
                title_pt: "col4",
                descr_df: "col5",
                descr_en: "col6",
                descr_fr: "col7",
                descr_pt: "col17",
                org: "col8",
                tpl_id: "col9",
                image: "col10",
                app_type: "col11",
                image_id: "col12",
                industry: "col13",
                industry_id: "col14",
                domain: "col15",
                api_key: "col16",
                cover: "col18",
                gkey: "col19",
                fbkey: "col20",
                ggkey: "col21",
                githkey: "col22",
                domain_payment: "col23",
                domain_app: "col24",
                domain_fintech: "col25",
                origin: "col26",
                lang: "col27",
                cloudfront_dist: "col28",
                mobile_id: "col29",
                mobile_url: "col30",
                version: "col31",
                state: 'col32'
            }
        }       
    }
}

function getSalesTx() {
    return {
        id:"558f6687ff9b4c17",
        fields: function() { 
            return {
                description: "col1",
                org: "col2",
                quantity: "col3",
                discount: "col4",
                total: "col5",
                source: "col6",
                context: "col7",
                object_id: "col8",
                product_label: "col9",
                product_id: "col10",
                payment_date: "col11",
                type: "col12",
                currency: "col13",
                customer_name: "col14",
                customer_email: "col15",
                customer_address: "col16",
                purchase_units: "col17",
                app_id: "col18",
                page_id: "col19",
                customer_phone: "col20",
                customer_id: "col21",
                price: "col22",
                subtotal: "col23",
                tax: "col24",
                billing_id: "col25",
                merchant_id: "col26",
                tx_id: "col27",
                user_id: "col28"
            }
        }       
    }
}

function getServiceCategory() {
    return {
        id:"15d91a1bd8674302",
        fields: function() { 
            return {
                code: "col1",
                label_df: "col2",
                label_en: "col3"
            }
        }       
    }
}

function getObjectTags() {
    return {
        id:"15d91a1bd8674302",
        fields: function() { 
            return {
                code: "col1",
                label_df: "col2",
                label_en: "col3",
                label_pt: "col4",
                label_fr: "col5",
                group: "col6",
                color: "col7",
                audience: "col8",
                context: "col9"
            }
        }       
    }
}

function getServiceTable() {
    return {
        id: "299c06e608254919",
        fields: function () {
            return {
                title: "col1",
                short_descr: "col2",
                long_descr: "col3",
                price: "col4",
                image: "col5",
                org_id: "col6",
                category: "col7",
                category_label: "col8",
                stype: "col9",
                context: "col10",
                image_id: "col11",
                currency: "col12",
                group_id: "col13",
                payment_url: "col14",
                invoice_payment_url: "col15",
                group_label: "col16",
                app_id: "col17",
                page_id: "col18",
                context_id: "col19",
                audience: "col20",
                state: "col21",
                product_page: "col22",
                product_descr: "col23",
                product_descr_html: "col24"
            }
        }
    }
}

function getInventoryTable() {
    return {
        id: "299c06e608254919",
        fields: function () {
            return {
                title: "col1",
                short_descr: "col2",
                long_descr: "col3",
                price: "col4",
                image: "col5",
                org_id: "col6",
                category: "col7",
                category_label: "col8",
                stype: "col9",
                context: "col10",
                image_id: "col11",
                currency: "col12",
                group_id: "col13",
                payment_url: "col14",
                invoice_payment_url: "col15",
                group_label: "col16",
                app_id: "col17",
                page_id: "col18",
                context_id: "col19",
                audience: "col20",
                state: "col21",
                product_page: "col22",
                product_descr: "col23",
                product_descr_html: "col24",
                unit: "col25",
                due_date: "col26",
                reference: "col27",
                old_price: "col28",
                quantity: "col29",
                manufact_id: "col30",
                manufact_name: "col31",
                manufact_img: "col32",
                comission: "col33",
                image_cache: "col34",
                owner: 'col35',
                owner_org: 'col36',
                parent_id: "col37",
                configuration: "col38",
                partner_id: "col39",
                partner_name: "col40",
                ut_id: "col41",
                ut_obj: "col42"
            }
        }
    }
}

function getBlogSystem() {
    return {
        id:"c0454ea7e8ef463c",
        fields: function() { 
            return {
                title_df: "col1",
                title_en: "col2",
                title_pt: "col3",
                content_df: "col4",
                content_en: "col5",
                content_pt: "col6",
                cover_picture: "col7",
                cover_picture_id: "col8",
                etags: "col9",
                webapp_id: "col10",
                page_id: "col11",
                section_id: "col12",
                state: "col13",
                content_mkd: "col14",
                post_type: "col15",
                comp_id: "col16",
                org_id: "col17",
                audience: "col18",
                pub_by_label: "col19",
                view_page: 'col20',
                view_pagerel: 'col21',
                loc_page_id: 'col22',
                context: "col23",
                context_id: "col24"
            }
        }       
    }
}

function getExpensesTx() {
    return {
        id:"228e6926f99e4819",
        fields: function() { 
            return {
                expense_date: "col1",
                item_id: "col2",
                total: "col3",
                event_id: "col4",
                context: "col5",
                event_description: "col6",
                currency: "col7",
                quantity: "col8",
                price: "col9",
                item_descr: "col10"
            }
        }       
    }
}

function getReviewsTable() {
    return {
        id:"e85cbc8a96014f58",
        fields: function() { 
            return {
                description: "col1",
                rating: "col2",
                context: "col3",
                object_id: "col4",
                audience: "col5",
                ddos_token: "col6",
                user_email: "col7",
                user_name: "col8",
                // user_id: "col9",
                user_phone: "col10",
                review_pin: "col11",
                app_id: "col12",
                page_id: "col13",
                comp_id: "col14",
                object_label: "col15",
                customer_id: "col16",
                org_id: "col17",
                user_id: "col18"
            }
        }       
    }
}

function getContestsTable() {
    return {
        id:"2ec9d1a3c1934ff7",
        fields: function() { 
            return {
                start_date: "col1",
                end_date: "col2",
                description: "col3",
                fb_link: "col4",
                email: "col5",
                phone_1: "col6",
                phone_2: "col7",
                phone_3: "col8",
                phone_4: "col9",
                title: "col10",
                cover: "col11",
                state: "col12",
                audience: "col13",
                welcome_msg: "col14",
                help_msg: "col15",
                support_group: "col16",
                allow_pub: "col17",
                allow_voting: "col18"
            }
        }       
    }
}

function getContestParticipantTable() {
    return {
        id:"754d8deacdf2437f",
        fields: function() { 
            return {
                name: "col1",
                email: "col2",
                phone: "col3",
                address: "col4",
                occupation: "col5",
                user_id: "col6",
                contest_id: "col7",
                signup_date: "col8",
                occupation_label: "col9",
                country_res: "col10",
                city_res: "col11",
                webapp_id: "col12"
            }
        }       
    }
}

function getGroupTopic() {
    return {
        id: "1100604287a34fbd",
        fields: function() { 
            return {
                group_id: "col1",
                title_df: "col2",
                title_en: "col3",
                description: "col4",
                description_en: "col5",
                tags: "col6",
                audience: "col7"
            }
        }       
    }
}

function getContestPoints() {
    return {
        id:"4079c2233b464456",
        fields: function() { 
            return {
                contest_id: "col1",
                participant_id: "col2",
                points: "col3",
                task_id: "col4",
                task_locale: "col5",
                task_description: "col6",
                is_dismiss: "col7"
            }
        }       
    }
}

function getContestManage() {
    return {
        id:"d66a715a86c84072",
        fields: function() { 
            return {
                contest_id: "col1",
                user_id: "col2",
                role: "col3"
            }
        }       
    }
}

function getVinti4TxResponse() {
    return {
        id:"4975b492a08e4adc",
        fields: function() { 
            return {
                message_type: "col1",
                merchantRespErrorCode: "col2",
                merchantRespErrorDetail: "col3",
                merchantRespErrorDescription: "col4",
                merchantRespMerchantRef: "col5",
                merchantRespMerchantSession: "col6",
                languageMessages: "col7",
                merchantRespAdditionalErrorMessage: "col8",
                result_fingerprint: "col9",
                result_fingerprint_version: "col10",
                ea_session_id: "col11",
                merchant_account_id: "col12",
                ea_token_id: "col13",
                timestamp: "col14",
                merchantRespCP: "col15",
                merchantRespTid: "col16",
                merchantRespMerchantRef_: "col17",
                merchantRespPurchaseAmount: "col18",
                merchantRespMessageID: "col19",
                merchantRespPan: "col20",
                merchantResp: "col21",
                merchantRespReloadCode: "col22",
                etx_merchant_id: "col23",
                billing_id: "col24",
                user_id: "col25"
            }
        }       
    }
}

function getBillingTbl() {
    return {
        id:"97bb223dca7f41d2",
        fields: function() { 
            return {
                customer_nif: "col1",
                customer_name: "col2",
                customer_address: "col3",
                payment_type: "col4",
                doc_type: "col5",
                payment_date: "col6",
                customer_phone: "col7",
                total: "col8",
                customer_surname: "col9",
                customer_postal_code: "col10",
                state: "col11",
                city: "col12",
                country: "col13",
                billing_ref: "col14",
                merchant_id: "col15",
                payment_tx_id: "col16",
                tax_id: "col17",
                context: "col18",
                context_id: "col19",
                state: "col20",
                user_id: "col21",
                customer_email: "col22",
                app_id: "col23",
                currency: "col24",
                auth_code: "col25",
                items: "col26",
                logo: "col27",
                comp_name: "col28",
                comp_email: "col29",
                comp_address: "col30",
                comp_nif: "col31",
                description: "col32",
                comp_phone: "col33",
                page_id: "col34",
                etx_comp_id: "col35",
                epay_host: "col36",
                referral_code: "col37",
                seq_number: "col38"
            }
        }       
    }
}

function getMerchantAccount() {
    return {
        id:"a5ac4d6f9a3c4506",
        fields: function() { 
            return {
                name: "col1",
                surname: "col2",
                phone: "col3",
                address: "col4",
                city: "col5",
                // state: "col6",
                postal_id: "col7",
                nif: "col8",
                organization_name: "col9",
                state: "col10",
                account_type: "col11",
                v4_posturl: "col12",
                v4_post_auth_code: "col13",
                v4_merchant_id: "col14",
                v4_currency: "col15",
                v4_lang: "col16",
                v4_response_url: "col17",
                v4_pos_id: "col20",
                email: "col18",
                country: "col19",
                description: "col21",
                payment_mode: "col22",
                phone_code: "col23",
                api_version: "col24"
            }
        }       
    }
}

function getEcommerceCart() {
    return {
        id:"a67c66b98b3a49fb",
        fields: function() { 
            return {
                etx_cookie_id: "col1",
                state: "col2",
                user_id: "col3",
                customer_id: "col4",
                name: "col5",
                surname: "col6",
                email: "col7",
                items: "col8",
                services: "col9",
                coupons: "col10",
                total: "col11",
                discount: "col12",
                coupon_code: "col13",
                auth_code: "col14",
                org_id: "col15",
                app_id: "col16",
                page_id: "col17",
                comp_id: "col18",
                user_phone: "col19",
                user_address: "col20",
                billing_address: "col21",
                source_link: 'col22',
                description: 'col23',
                ret_customer: "col24",
                is_new: 'col25',
                delivery_method: 'col26',
                state_updater: 'col27',
                state_update_date: 'col28',
                j1n_key: "col29",
                j1n_tx_id: "col30",
                payment_method: "col31",
                client_source: "col32",
                referral_code: "col33",
                postal_code: "col34",
                delivery_date: "col35",
                delivery_post: "col36",
                region: "col37",
                shipping_address: "col38",
                shipping_region: "col39",
                delivery_status: "col40",
                seller_id: "col41",
                seller_name: "col42",
                seller_org: "col43"
            }
        }       
    }
}

function getCartDelivery() {
    return {
        id:"b4b7979e23bb45c9",
        fields: function() { 
            return {
                cart_id: "col1",
                delivery_date: "col2",
                concelho: "col3",
                ilha: "col4",
                name: "col5",
                email: "col6",
                delivery_by: "col7",
                delivery_method: "col8",
                annexes: "col9",
                billing_id: "col10",
                delivery_obs: "col11",
                delivery_address: "col12",
                user_id: "col13",
                tracking_code: "col14"
            }
        }       
    }
}

function getWebAppAccess() {
    return {
        id:"41aa48e19ae34320",
        fields: function() { 
            return {
                etx_cookie_id: "col1",
                access_ts: "col2",
                user_lang: "col3",
                user_ip: "col4",
                user_country: "col5",
                referrer: "col6",
                context: "col7",
                context_id: "col8",
                url: "col9",
                url_complete: "col10",
                app_id: "col11",
                page_id: "col12",
                org_id: "col13",
                client: "col14",
                user_id: "col15",
                appClient: "col16"
            }
        }       
    }
}

function getIPLogTable() {
    return {
        id:"318e15928fa34373",
        fields: function() { 
            return {
                etx_cookie_id: "col1",
                ip_data: "col2",
                ip: "col3",
                url: "col4",
                client: "col5"
            }
        }       
    }
}

function getAppAsset() {
    return {
        id:"61eafeff05b54937",
        fields: function() { 
            return {
                name: "col1",
                content: "col2",
                type: "col3",
                permission_conf: "col4",
                webapp_id: "col5",
                page_id: "col6",
                comp_id: "col7",
                policy: "col8"
            }
        }       
    }
}

function getAuction() {
    return {
        id:"449bb31e53df4b87",
        fields: function() { 
            return {
                title: "col1",
                description: "col2",
                start_amount: "col3",
                min_req_amount: "col4",
                start_time: "col5",
                end_time: "col6",
                picture: "col7",
                terms: "col8",
                state: "col9",
                group_id: "col10"
            }
        }       
    }
}

function getAuctionBid() {
    return {
        id:"088141dcde324873",
        fields: function() { 
            return {
                auction_id: "col1",
                amount: "col2",
                user_name: "col3",
                user_id: "col4",
                user_email: "col5",
                etx_cookie_id: "col6",
                subscriber_id: "col7",
                user_phone: "col8"
            }
        }       
    }
}

function getNotificationsTbl() {
    return {
        id:"925690651ebe4fbf",
        fields: function() { 
            return {
                notify_to: "col1",
                notifier: "col2",
                time: "col3",
                event: "col4",
                channel: "col5",
                state: "col6",
                message: "col7",
                message_subject: "col8",
                frequency: "col9",
                deliveries: "col10",
                context_id: "col11",
                context: "col12",
                app_id: "col13",
                page_id: "col14",
                comp_id: "col15",
                merchant_id: "col16",
                attachments: "col17",
                tags: "col18",
                group_id: "col19",
                user_id: "col20",
                severity: "col21"
            }
        }       
    }
}

function getFChainTokenTx() {
    return {
        id:"a6d72967bf514772",
        fields: function() { 
            return {
                token_id: "col1",
                from: "col2",
                to: "col3",
                transaction_type: "col4",
                amount: "col5",
                message: "col6",
                signature: "col7",
                transaction_ref: "col8",
                block_ref: "col9",
                chain_ref: "col10",
                contract_ref: "col11",
                lock_id: "col12",
                lock_context: "col13",
                parent_tx: "col14",
                lock_time: "col15"
            }
        }       
    }
}

function getFChainToken() {
    return {
        id:"5f76a26b45d94e6c",
        fields: function() { 
            return {
                description: "col1",
                name: "col2",
                ticker: "col3",
                initial_supply: "col4",
                token_org: "col5",
                chain: "col6",
                owner: "col7",
                logo: "col8",
                transaction_ref: "col9",
                block_ref: "col10",
                chain_ref: "col11",
                contract_ref: "col12"
            }
        }       
    }
}

function getProductMedia() {
    return {
        id:"44e87b7ce1fb4f18",
        fields: function() { 
            return {
                description: "col1",
                product_id: "col2",
                context: "col3",
                annex_id: "col4",
                url: "col5",
                store_type: "col6"
            }
        }       
    }
}

function getManufacturer() {
    return {
        id:"f7866e1e22584b92",
        fields: function() { 
            return {
                name: "col1",
                description: "col2",
                logo: "col3",
                logo_small: "col4",
                website: "col5",
                categories: "col6",
                owner: "col7"
            }
        }       
    }
}

function getZMessaging() {
    return {
        id:"f7243841d09b4e12",
        fields: function() { 
            return {
                level: "col14",
                from_id: "col1",
                from_email: "col2",
                context: "col5",
                context_id: "col6",
                org_id: "col3",
                app_id: "col4",
                compiled_msg: "col7",
                vhtml_msg: "col8",
                subject: "col9",
                lang: "col10",
                recipients: "col20",
                categories: "col13",

                to_id: "col11",
                to_email: "col12",
                last_wstate: "col15",
                last_wupdate: "col16",
                last_worker: "col17",
                last_wmsg: "col18",
                state_changes: "col19",
            }
        }       
    }
}

function getJ1NOperations() {
    return {
        id:"ca8ce488f43847f9",
        fields: function() { 
            return {
                transactions: "col1",
                context: "col2",
                context_id: "col3",
                date: "col4",
                j1n_key: "col5",
                description: "col6",
                user_id: "col7",
                user_name: "col8",
                user_org: "col9"
            }
        }       
    }
}

function getExperiencesTbl() {
    return {
        id:"53b9c07a56554263",
        fields: function() { 
            return {
                title: "col1",
                image: "col2",
                price_amount: "col3",
                currency: "col4",
                description: "col5",
                location: "col6",
                category: "col7",
                state: "col8",
                merchant_id: "col9",
                min: "col10",
                max: "col11",
                start_date: "col12",
                end_date: "col13",
                reservation_status: "col14",
                min_part: "col15",
                max_part: "col16",
                host_id: "col17",
                allow_dates: "col18"
            }
        }       
    }
}

function getReservationTbl() {
    return {
        id:"e1ccc7b182ae4b39",
        fields: function() { 
            return {
                customer_name: "col15",
                customer_surname: "col16",
                phone: "col17",
                email: "col18",
                address: "col19",
                nif: "col20",
                country: "col21",
                experience_id: "col1",
                description: "col2",
                amount: "col3",
                final_price: "col4",
                items: "col5",
                start_date: "col6",
                end_date: "col7",
                discount: "col8",
                payment_id: "col9",
                billing_id: "col10",
                state: "col11",
                currency: "col12",
                app_id: "col13",
                page_id: "col14",
                merchant_id: "col22",
                user_id: "col23",
                phone_code: "col24",
                country_code: "col25",
                appclient: "col26",
                client_ip: "col27",
                cookie_id: "col28",
                client_loc: "col29",
                host_id: "col30",
                corporate_id: "col31"
            }
        }       
    }
}

function getEntityObj() {
    return {
        id:"e2cb3b3ba03d4c5e",
        fields: function() { 
            return {
                app_id: "col1",
                org_id: "col2",
                ent_name: "col3",
                date: "col4",
                ent_data: "col5",
                context: "col6",
                type: "col7",
                ent_ref_id: "col8"
            }
        }       
    }
}

function getHostTbl() {
    return {
        id:"63b095693e69472d",
        fields: function() { 
            return {
                title: "col1",
                name: "col2",
                surname: "col3",
                location: "col4",
                picture: "col5",
                cover: "col6",
                org_id: "col7",
                email: "col8",
                phone: "col9",
                address: "col10",
                tags: "col11",
                description: "col12",
                country: "col13",
                lang: "col14",
                media_list: 'col15',
                user_id: "col16",
                state: "col17",
                msg: "col18",
                emailf: "col19",
                app_id: "col20",
                fb_link: "col21",
                insta_link: "col22",                
                host_reference: "col25"
            }
        }       
    }
}

function getPaymentPlan() {
    return {
        id:"538b38e4bf594a53",
        fields: function() { 
            return {
                name: "col1",
                description: "col2",
                price: "col3",
                currency: "col4",
                features: "col5",
                org_id: "col6",
                app_id: "col7",
                unit: "col8",
                image: "col9",
                context: "col10",
                context_id: "col11",
                merchant_id: "col12"
            }
        }       
    }
}

// function function_name (argument) {
//     // body...
// }

function getETX_Plans () {
    return {
        GOLD: '69a961ad72d041be',
        BASIC: '8482c207329b4ce2',
        SILVER: '1dfba0dd0cd94e1b',
        CONSUMER_ACCOUNT: 'c8796073d6104379',
    };
}

function getNTWPosts() {
    return {
        id:"5e7ae3d7555d44b0",
        fields: function() { 
            return {
                author_name: "col1",
                author_picture: "col2",
                title: "col3",
                description: "col4",
                timestamp: "col5",
                images: "col6",
                location: "col7",
                privacy: "col8",
                sponsor_id: "col9",
                link: "col10",
                author_id: "col11",
                author_type: "col12",
                app_id: "col13",
                org_id: "col14",
                post_type: "col15",
                share_post_id: "col16",
                group_id: "col17",
                group_name: "col18",
                post_media_type: "col19",
                state: 'col20',
                post_preview: "col21",
                expiration: "col22",
                publish_time: "col23"
            }
        }
    }
}

function getNTWComment() {
    return {
        id:"5b682c1702884fce",
        fields: function() { 
            return {
                description: "col1",
                author_name: "col2",
                author_id: "col3",
                author_picture: "col4",
                location: "col5",
                object_id: "col6",
                images: "col7",
                org_id: "col8",
                app_id: "col9"
            }
        }       
    }
}

function getCorporateTbl() {
    return {
        id:"ffef1f341b60433f",
        fields: function() { 
            return {
                nome: "col1",
                email: "col2",
                address: "col3",
                bank_account: "col4",
                bank: "col5",
                code: "col6",
                user_id: "col7",
                org_id: "col8",
                app_id: "col9",
                comission: "col10",
                state: "col11",
                msg: "col12",
                emailf: "col13",
                phone: "col14"
            }
        }       
    }
}

function getUtIntGEvents() {
    return {
        id:"4774737f2c13469c",
        fields: function() { 
            return {
                user_id: "col1",
                billing_id: "col2",
                ev_context: "col3",
                ev_context_id: "col4",
                date: "col5",
                value: 'col6',
                data: 'col7'
            }
        }       
    }
}

var conf = {
    ETX_PLANS: getETX_Plans,
    HostTbl: getHostTbl,
    appAPI: getApi,
    apiHost: getApiHost,
    authAPI: getAuthApi,
    metricsAPI: getMetricsHost,
    OBJ_STATE: OBJ_STATE,
    ExperiencesTbl: getExperiencesTbl,
    ReservationTbl: getReservationTbl,
    PaymentPlan: getPaymentPlan,
    NWPost: getNTWPosts,
    NTWComment: getNTWComment,
    CorporateAccount: getCorporateTbl,
    UT_INTG: getUtIntGEvents,

    ETX_MARKET: function () {
        return '66378961f2d24212'
    },

    // SalesTx: getSalesTx,
    AEROSPIKE_HOST: function () {
        return '127.0.0.1:4000';
    },
    SOLR_HOST: function () {
        // etx-messaging-ip: (172.31.17.115)
        return 'http://127.0.0.1:8983';
    },
    ZOBJ_SOLR_HOST: function () {
        // etx-messaging-ip: (172.31.17.115)
        return 'http://127.0.0.1:8983';
    },
    VSTREAM_SOLR_HOST: function () {
        // etx-messaging-ip: (172.31.17.115)
        return 'http://127.0.0.1:8983';
    },
    SOLR_HOME: function () {
        // etx-messaging-ip: (172.31.17.115)
        return '/home/ayrton/env/git/Infra/solr-6.5.0';
    },
    AMQP_HOST: function () {
        return 'amqp://guest:guest@127.0.0.1';
    },
    AMPQ_HOSTIP: function () {
        return '127.0.0.1';
    },
    AMQP_USER: function () {
        return {
            u: 'guest',
            p: 'guest'
        }
    },
    START_DATE: function (params) {
        return "2018-01-01 00:00:00";
    },
    END_DATE: function () {
        return "2100-12-31 00:00:00";
    },
    UT_HOST: function () {
        return 'http://esbdev.tmais.cv:8280/playit';
    },
    KLH_PATH: function () {

        return "https://kualeh.enterstarts.com/index.php";
    },
    UT_AUTH: function () {
        return {
            "UserName": "Kualeh",
            "Password": "Ku@l3h@Un!t3l",
            "CanalCode": "1028",
            "RolesName": "Kualeh"
        };
    },
    ETX_PUBLIC_IP: function () {
        return "http://127.0.0.1:9000";
    },
    ETX_PATH: function () {
        return "/home/ayrton/env/git/99app/etx-public";
    },
    PROJECT_HOME: function () {
        return "/home/ayrton/env/git/99app/etx-v2/etx-marko-v1/src";
    },
    WEBAPP_SANDBOX_PATH: function () {
        return "/home/ayrton/env/git/99app/etx-webapp-sandbox";
    },
    MOBILE_SANDBOX_PATH: function () {
        return "/home/ayrton/env/git/99app/etx-mobile-sandbox";
    },
    MOBILE_LIB: function () {
        return "/home/ayrton/env/git/99app/etx-mobile-public";
    },
    APP_PATH: function () {
        return "http://"+host+":3003"
    },
    LIGHT_PATH: function () {
        return "https://cliente.enterstarts.com/index.php";
    },
    UNITELP_PATH: function () {
        return "https://uniteltmais.enterstarts.com/index.php";
    },
    STATIC_PATH: function () {
        return "http://"+host+":9000";
    },
    WsHost: function() {
        return "http://"+host+":9000";        
    },
    PUBLICA_HOST_CDN: function () {
        // TODO: work on
        return "http://127.0.0.1:9000";
        // return "https://d2aeipzem347xy.cloudfront.net";
    },
    PUBLICA_IMAGE_CDN: function () {
        // TODO: work on
        // return "https://publica.enterstarts.com";
        // return "https://d2aeipzem347xy.cloudfront.net";
        return "https://d315x6rrkxxeu2.cloudfront.net";
    },
    S3_HOST_CDN: function () {
        return "https://d1wndnojn2tgxv.cloudfront.net";
    },
    MOBILE_CDN_VERSION: function () {
        return '';
    },
    WEB_CDN_VERSION: function () {
        return '';
    },

    // ETX CONF TABLES
    EntityObj: getEntityObj,
    J1NOperations: getJ1NOperations,
    Manufacturer: getManufacturer,
    FChainToken: getFChainToken,
    FChainTokenTx: getFChainTokenTx,
    Vinti4TxResponse: getVinti4TxResponse,
    EntityTable: getEntityTable,
    ServiceReqTable: getServiceRequestTbl,
    ServiceCategory: getServiceCategory,
    ServiceTbl: getServiceTable,
    SalesTx: getSalesTx,
    ObjectTags: getObjectTags,
    BlogSystem: getBlogSystem,
    ContestTable: getContestsTable,
    ContestParticipant: getContestParticipantTable,
    GroupTopic: getGroupTopic,
    ExpensesTx: getExpensesTx,
    InventoryTable: getInventoryTable,
    Billing: getBillingTbl,
    MerchantAccount: getMerchantAccount, 
    EcommerceCart: getEcommerceCart,
    CartDelivery: getCartDelivery,
    IPLogTbl: getIPLogTable,
    AppAsset: getAppAsset,
    Auction: getAuction,
    AuctionBid: getAuctionBid,
    ProductMedia: getProductMedia,
    ZMessaging: getZMessaging,

    ReviewsTable: getReviewsTable,
    ContestPoints: getContestPoints,
    ContestManage: getContestManage,
    Notification: getNotificationsTbl,

    CustomerTable: getCustomerTable,
    AnnexTable: getFilestore,
    Filestore: getFilestore,
    WebAppTable: getWebAppTbl,
    WebAppTplTable: getWebAppTplTable,
    WebAppPage: getWebAppPage,
    WebPageSections: getWebAppPage_Sections,
    WebPageComp: getWebPage_Comp,
    WebappAccess: getWebAppAccess,

    // ARFA CONF TABLES
    SugestionTable: getSugestionTable,
    ComplaintTable: getComplaintTable,
    CompbookTable: getCompbookTable,
    IReclamationTable: getIReclamation,

    AccountsTable: getAccountsTable,
    MessageTable: getMessagesTable
};


exports.conf = conf;
