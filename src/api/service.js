const fetch=require('node-fetch');
const modsConf = require('./modules-conf');

// var host = 'http://192.168.0.100:5000';
var host = modsConf.c.apiHost();
const defaultAPI = host+'/api/v1';

function $s(endpoint, token, api) {
    this.endpoint = endpoint;
    this.token=token;
    if (api) {
        this.api = api;
    } else {
        this.api = defaultAPI;
    }
};

$s.prototype.all = function(filter) {
    var ftfilters='';

    if (filter&&filter.limit) {
        ftfilters+='&limit='+filter.limit;
    }

    if (filter&&filter.offset!==undefined) {
        ftfilters+='&offset='+filter.offset;
    }

    var qs='';
    if (!this.endpoint[this.endpoint.length-1]=='?') {
        qs='?';
    }

    var req={
        method: 'get',
        headers: {
            'Content-type': 'application/json',
            'Authorization': this.token
        }
    };

    return fetch(this.api+'/'+this.endpoint+qs+ftfilters, req);
};

$s.prototype.get = function(id, filter) {
    var req={
        method: 'get',
        headers: {
            'Content-type': 'application/json',
            'Authorization': this.token
        }
    };

    // return fetch(, req)
    // .then(function (Resp){
    //     return Resp.json();
    // });
    var url=this.api+'/'+this.endpoint+"/"+id;
    return new Promise(function (resolve, reject) {
        fetch(url, req)
        .then(function (Resp){
            Resp.json()
            .then(function (Data) {
                if (Resp.status!=200 &&
                    Resp.status!=201 &&
                    Resp.status!=204 ) {
                    reject(Data);
                } else {
                    resolve(Data);
                }
            }, reject);
        }, reject);
    });
};

$s.prototype.fetch = function(end, filter) {
    var req={
        method: 'get',
        headers: {
            'Content-type': 'application/json',
            'Authorization': this.token
        }
    };

    var url=this.api+'/'+this.endpoint+end;
    return new Promise(function (resolve, reject) {
        fetch(url, req)
        .then(function (Resp) {
            Resp.json()
            .then(function (Data) {
                if (Resp.status!=200&&Resp.status!=201&&Resp.status!=204) {
                    reject(Data);
                } else {
                    resolve(Data);
                }
            }, reject);
        }, reject);
    });
};

$s.prototype.post = function(end, data) {
    var url=this.api+'/'+this.endpoint+"/"+end;
    var req={
        method: 'post',
        headers: {
            'Content-type': 'application/json;charset=UTF-8',
            'Authorization': this.token
        },
        body: JSON.stringify(data)
    };

    return new Promise(function (resolve, reject) {
        fetch(url, req)
        .then(function (Resp){
            Resp.text()
            .then(function (textResp) {
                try {
                    var Data=JSON.parse(textResp);
                    if (Resp.status!=200&&Resp.status!=201&&Resp.status!=204) {
                        reject(Data);
                    } else {
                        resolve(Data);
                    }
                } catch (e) {
                    // console.info("textResp: ", textResp);
                    // reject(Resp);
                    if (Resp.status!=200&&Resp.status!=201&&Resp.status!=204) {
                        reject(textResp);
                    } else {
                        resolve(textResp);
                    }
                }

            }, reject);
        }, reject);
    });
}

$s.prototype.saveNew = function(data) {
    var url=this.api+'/'+this.endpoint;
    var method="post";

    if (data.id) {
        url += '/'+data.id;
        method = "put";
    } else {
        url += '/new';
    }

    var req={
        method: method,
        headers: {
            'Content-type': 'application/json',
            'Authorization': this.token
        },
        body: JSON.stringify(data)
    };

    return new Promise(function (resolve, reject) {
        fetch(url, req)
        .then(function (Resp){
            Resp.json()
            .then(function (Data) {
                if (Resp.status!=200&&Resp.status!=201&&Resp.status!=204) {
                    reject(Data);
                } else {
                    resolve(Data);
                }
            }, reject);
        }, reject);
    });
};

$s.prototype.save = function(data) {
    var url=this.api+'/'+this.endpoint;
    var method="post";

    if (data.id) {
        url += '/'+data.id;
        method = "put";
    }

    var req={
        method: method,
        headers: {
            'Content-type': 'application/json',
            'Authorization': this.token
        },
        body: JSON.stringify(data)
    };

    return new Promise(function (resolve, reject) {
        fetch(url, req)
        .then(function (Resp){
            Resp.json()
            .then(function (Data) {
                if (Resp.status!=200&&Resp.status!=201&&Resp.status!=204) {
                    reject(Data);
                } else {
                    resolve(Data);
                }
            }, reject);
        }, reject);
    });
};

$s.prototype.delete = function(id) {
    var url=this.api+'/'+this.endpoint+'/'+id;
    var req={
        method: 'DELETE',
        headers: {
            'Content-type': 'application/json',
            'Authorization': this.token
        },
    };

    return new Promise(function (resolve, reject) {
        fetch(url, req)
        .then(function (Resp) {
            // console.info("Resp: ", Resp);
            if (Resp.status!=200&&Resp.status!=201&&Resp.status!=204) {
                Resp.json().then(
                    reject, 
                    reject
                );
            } else {
                resolve({
                    code: 200, 
                    status: 200, 
                    msg: "object{"+id+"} deleted"
                });
            }
        }, reject);
    });
};

function fetchRows(AuthToken, schemaId, mapper, querycols, queryfilters) {
    if (!schemaId) {
        throw ("Please provide schemaId");
    }

    if (!mapper) {
        throw ("Please provide mapper{limit, offset}");
    }

    if (!querycols) {
        throw ("Please provide querycols[col1, col2, ...]");
    }

    var comp=JSON.stringify(queryfilters||{});

    var url = "filter/"+schemaId+
        "?fields="+encodeURIComponent(comp)+
        "&sort_by=created_at desc"+
        "&cols="+querycols+
        "&offset="+mapper.offset+
        "&limit="+mapper.limit;

    var api=new $s("object", AuthToken);
    return api.get(url);
}

exports.apiService = $s;
exports.APIService = $s;
exports.fetchRows = fetchRows;
