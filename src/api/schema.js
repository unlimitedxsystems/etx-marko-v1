#!/usr/bin/env node
const assert = require('assert');
const fetch=require('node-fetch');
const fs = require('fs');
const service = require('./service');
const modsConf = require('./modules-conf');

const TOKEN_FILE='.app-token';
// var host = 

// function getToken() {
//     var promise=new Promise(function (resolve, reject) {
//         fs.readFile(TOKEN_FILE, 'utf-8' ,function (err, buf) {
//             var content=buf;
//             if (!content) {
//                 return loginAndSaveToken();
//             } else {
//                 var auth=JSON.parse(buf.toString());
//                 me(auth.reqToken)
//                 .then(function(){
//                     resolve(auth);
//                 }, function (err) {
//                     console.log('[WARN] Bad token. renewing');
//                     return loginAndSaveToken();
//                 }, reject);
//             }
//         });
//     });

//     return promise;
// }

// function loginAndSaveToken() {
//     return new Promise(function (resolve, reject){    
//         login(user)
//         .then(function (auth) {
//             fs.writeFile(TOKEN_FILE, JSON.stringify(auth), function(err, data){
//                 if (err) {
//                     console.log('[ERROR] %o', err);
//                     process.exit(1);
//                 }
//                 console.log("[INFO] saved token.");
//                 resolve(auth);
//             }, reject);
//         }, reject);
//     })
// }

// function login(userData) {
//     return new Promise(function (resolve, reject) {
//         fetch(host+'/api/v1/user/signin', {
//             method:'POST',
//             headers: {'Content-type': 'application/json'},
//             body: JSON.stringify(userData)
//         }).then(function (Data){
//             Data.json().then(function (Resp){
//                 if (Resp.token) {
//                     resolve(Resp);
//                 } else {
//                     console.error('[ERROR] could not login, req: %o', userData);
//                     console.error('[ERROR] Details %o', Resp);
//                     reject(Resp);
//                 }
//             }, reject);
//         }, reject);
//     })
// }

// function me(token) {
//     return new Promise(function (resolve, reject) {
//         fetch(host+'/api/v1/user/me', {
//             method:'GET',
//             headers: {'Content-type': 'application/json',
//                      'Authorization': token},
//         }).then(function (Data){
//             Data.json()
//             .then(function (Resp) {
//                 resolve();
//             }, reject);
//         }, reject);
//     })
// }

function fetchSchema (Auth, id, conf_) {
    return new Promise(function (resolve, reject) {
        // body...
        var host;
        if (conf_ && conf_.host){
            host=conf_.host;
        } else {
            host=modsConf.c.apiHost();
        }

        var url=host+'/api/v1/schema/'+id;
        console.info("host: ", host);
        console.info("url:  ", url);

        fetch(url, {
            method:'GET',
            headers: {
                'Content-type': 'application/json',
                'Authorization': Auth.reqToken,
            }
        }).then(function (Data){
            Data.json().then(function (Resp){
                resolve(Resp);
            }, reject);
        }, reject);
    });
}

function fetchFields(Auth, id, conf_) {
    // body...
    return new Promise(function (resolve, reject) {
        // body...
        var host;
        if (conf_ && conf_.host){
            host=conf_.host;
        } else {
            host=modsConf.c.apiHost();
        }

        var url=host+'/api/v1/schema/'+id+'/fields';
 
        fetch(url, {
            method:'GET',
            headers: {
                'Content-type': 'application/json',
                'Authorization': Auth.reqToken,
            }
        }).then(function (Data){
            Data.json().then(function (Resp){
                resolve(Resp);
            }, reject);
        }, reject);
    });
}

function createSchema(Auth, obj, conf_) {
    // body...
    return new Promise(function (resolve, reject) {
        var host;
        if (conf_ && conf_.host){
            host=conf_.host;
        } else {
            host=modsConf.c.apiHost();
        }

        var url=host+'/api/v1/schema/new';
        console.info("host: ", host);
        console.info("url:  ", url);

        fetch(url, {
            method:'POST',
            headers: {
                'Content-type': 'application/json',
                'Authorization': Auth.reqToken
            },
            body: JSON.stringify(obj)
        }).then(function (Data){
            Data.json().then(function (Resp) {
                if (Resp.id) {
                    resolve(Resp);
                } else {
                    reject(Resp);
                }
            }, reject);
        }, reject);
    });
}

function addField (Auth, schemaId, field, conf_) {
    return new Promise(function (resolve, reject) {
        var host;
        if (conf_ && conf_.host){
            host=conf_.host;
        } else {
            host=modsConf.c.apiHost();
        }

        var url=host+'/api/v1/schema/'+schemaId+'/fields';

        fetch(url, {
            method:'POST',
            headers: {
                'Content-type': 'application/json',
                'Authorization': Auth.reqToken
            },
            body: JSON.stringify(field)
        }).then(function (Data){
            Data.json().then(function (Resp) {
                if (Resp.id) {
                    resolve(Resp);
                } else {
                    reject(Resp);
                }
            }, reject);
        }, reject);
    });
}

function archiveSchema (Auth, schemaId, conf_) {
    return new Promise(function (resolve, reject) {
        var host;
        if (conf_ && conf_.host){
            host=conf_.host;
        } else {
            host=modsConf.c.apiHost();
        }

        var url=host+'/api/v1/schema/'+schemaId;

        fetch(url, {
            method:'DELETE',
            headers: {
                'Content-type': 'application/json',
                'Authorization': Auth.reqToken
            },
        }).then(function (Data){
            if (Data.statusText=='No Content') {
                resolve();
            } else {
                reject(Data);
            }
        }, reject);
    });
}

// exports.signin = login;
// exports.me = me;
exports.fetchById=fetchSchema;
exports.create=createSchema;
exports.fieldsOf=fetchFields;
exports.addField=addField;
exports.archive=archiveSchema;